import axios from './__includes__/axios'

export default class ApiRequest {

    static login(email, password){
        return axios.post('security/login', {
            email: email,
            password: password  
        })
    }

    static register(email, password){
        return axios.post('security/register', {
            email: email,
            password: password
        })
    }

    static logout() {
        return axios.post('security/logout')
    }

    static getAccount() {
        return axios.get('/user')
    }

    static updateAccount(username, email, firstname, lastname) {
        return axios.post('/user', {
            username: username,
            email: email,
            firstname: firstname,
            lastname: lastname
        })
    }

    static changePasswordAccount(newPassword) {
        return axios.post('/user', {
            password: newPassword,
            "first_connection": false
        })
    }

    static getUserClubs(id) {
        return axios.get('/users/'+id+'/clubs')
    }
    
    static getOneClub(id) {
        return axios.get('/clubs/'+id)
    }

    static getClubSubscriptions(id) {
        return axios.get('/stripe/'+id+'/plans')
    }

    static setClubSubscription(clubId, token, name) {
        return axios.post('/stripe/'+clubId+'/subscriptions', {
            "id": clubId,
            "token": token,
            "plan_name": name
        })
    }

    static getSessionClub(id, day, userId) {
        return axios.get('/clubs/'+id+'/sessions?day='+day+'&userId='+userId)
    }

    static getReservation(clubId, sessionId) {
        return axios.get('/clubs/'+clubId+'/sessions/'+sessionId)
    }

    static setReservation(clubId, sessionId) {
        return axios.post('/clubs/'+clubId+'/sessions/'+sessionId+"/reservations")
    }

    static removeReservation(clubId, sessionId) {
        return axios.delete('/clubs/'+clubId+'/sessions/'+sessionId+"/reservations/0?mobileApp")
    }

    static pushNotifToken(token, previousToken) {
        return axios.post('/push-notification-tokens', {
            "token": token,
            "previous_token": previousToken
        })
    }
}