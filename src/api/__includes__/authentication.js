import { AsyncStorage } from 'react-native'

export const checkUserAuth = async () => {
    return (await AsyncStorage.getItem('accessToken') !== null) // Return true if user has a token saved
}

export const userToken = async () => {
    return await AsyncStorage.getItem('accessToken')
}

export const updateToken = async (axios) => {
    let response = await axios({
        method: 'post',
        url: '/security/token/refresh',
        data: {
            refreshToken: userRefreshToken()
        }
    })

    response = response.data

    const {accessToken, refreshToken} = response.data
    userAuth(accessToken, refreshToken)
    return accessToken
}

export const userRefreshToken = async () => {
    return await AsyncStorage.getItem('refreshToken')
}

export const userAuth = async (accessToken, refreshToken) => {
    await AsyncStorage.setItem('accessToken', accessToken)
    await AsyncStorage.setItem('refreshToken', refreshToken)
}

export const userLogin = async (email, password) => {
    await AsyncStorage.setItem('email', email)
    await AsyncStorage.setItem('password', password)
}

export const userLogout = async () => {
    await AsyncStorage.clear()
}