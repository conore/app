import axios from 'axios'
import {userToken, updateToken, userLogout} from "./authentication";

const HEADER_AUTH_TOKEN = 'X-CONORE-AUTH-TOKEN'

axios.defaults.baseURL = 'https://dev-api.conore.com/'
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json'

/* -------------------------
   ----- INTERCEPTORS ------
   ------------------------- */

// Request
axios.interceptors.request.use(async (config) => {
    /** In dev, intercepts request and logs it into console for dev */
    //console.info("⬅️️ Request ✅", config)
    config.headers[HEADER_AUTH_TOKEN] = await userToken()

    return config
}, (error) => {
    throw error
});

// Response
axios.interceptors.response.use(async (response) => {

    //console.info("⬅️️ Response ✅", response)

    if(response.data.message === 'token_has_expired'){
        return updateToken(axios).then((accessToken) => {

            response.config.headers[HEADER_AUTH_TOKEN] = accessToken
            return axios.request(response.config)
        })
    }
    if(response.data.message === 'invalid_credentials'){
        await userLogout()
    }

    if(response.data.status !== "success") throw response.data.message

    return response
}, (error) => {
    throw error
});


export default axios