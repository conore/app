import messages from '../../assets/messages/messages.js'

export default getMessage = (index) => {
    return (messages[index] == undefined) ? index : messages[index]
}
