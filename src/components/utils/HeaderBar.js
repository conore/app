import React, { Component } from 'react'
import { View, Text, BackHandler, TouchableOpacity } from 'react-native'
import styles from '../../styles/HeaderBar'

export default class HeaderBar extends Component {

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.activeBtn()
        })
    }

    handlerSwitchClub() {
        this.props.activeBtn()
    }

    render() {
        if(this.props.activeBtn != undefined) {
            return (
                <TouchableOpacity  onPress={() => this.handlerSwitchClub()} style={styles.topBar}>
                    <Text style={styles.topBarTxt}>{this.props.title}</Text>
                </TouchableOpacity>
            )
        }
        else {
            return (
                <View style={styles.topBar}>
                    <Text style={styles.topBarTxt}>{this.props.title}</Text>
                </View>
            )
        }
    }
}
