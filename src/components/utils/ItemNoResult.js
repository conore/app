import React, {Component} from 'react'
import { Text, View } from 'react-native'
import SvgUri from 'expo-svg-uri'

import styles from '../../styles/ItemNoResult'

export default function ItemNoResult(props) {

    return (
        <View style={styles.item}>
            <View style={styles.noResult}>
                <SvgUri width={70} style={{alignItems: 'center'}} source={(props.img === "calendar") ? require('../../assets/icon/errors/icon-reserver-active.svg') : require('../../assets/icon/errors/icon-clubs-active.svg')} />
                <Text style={styles.noResultTxt}>{props.msg}</Text>
                <Text style={styles.retry}>Revenez plus tard</Text>
            </View>
        </View>
    )

}
