import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import styles from '../../styles/LoadingView'

export default function LoadingView(props) {

    if(props.isLoading) {
        return (
            <View style={styles.loading_container}>
                <ActivityIndicator color="#5e72e4" size="large"/>
            </View>
        )
    }
    else {
        return null
    }
    
}