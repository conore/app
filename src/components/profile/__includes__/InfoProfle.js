import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native'
import SvgUri from 'expo-svg-uri'
import styles from '../../../styles/InfoProfil'

export default class InfoProfile extends Component {

    handlerView(view, data) {
        this.props.handlerView(view, data)
    }

    render() {
        const { clubs } = this.props

        return(
            <View style={styles.view}>
                <View style={styles.bottomBlock}>
    
                    <TouchableOpacity activeOpacity={1} onPress={() => this.handlerView("ListClubs", null)} style={styles.clubsBlock}>
                        <View style={styles.clubsTitle}>
                            <SvgUri width={25} height={60} source={require('../../../assets/icon/errors/icon-clubs-active.svg')} /> 
                            <Text style={styles.label}>Vos clubs</Text>
                        </View>
                        <SvgUri style={styles.arrowBtn} width={15} source={require('../../../assets/icon/profil/arrow-right.svg')} />
                    </TouchableOpacity>
                    
                    <TouchableOpacity activeOpacity={1} onPress={() => this.handlerView("ListReservations", {clubs: clubs})} style={styles.settingsBlock}>
                        <View style={styles.clubsTitle}>
                            <SvgUri width={20} height={60} source={require('../../../assets/icon/profil/reservation.svg')} />
                            <Text style={styles.label}>Vos réservations</Text>
                        </View>
                        <SvgUri style={styles.arrowBtn} width={15} source={require('../../../assets/icon/profil/arrow-right.svg')} />
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={1} onPress={() => this.handlerView("Subscriptions", null)} style={styles.settingsBlock}>
                        <View style={styles.clubsTitle}>
                            <SvgUri width={25} height={60} source={require('../../../assets/icon/profil/euro.svg')} />
                            <Text style={styles.label}>Vos abonnements</Text>
                        </View>
                        <SvgUri style={styles.arrowBtn} width={15} source={require('../../../assets/icon/profil/arrow-right.svg')} />
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={1} onPress={() => this.handlerView("Settings", null)} style={styles.settingsBlock}>
                        <View style={styles.clubsTitle}>
                            <SvgUri width={25} height={60} source={require('../../../assets/icon/profil/settings.svg')} />
                            <Text style={styles.label}>Paramètres</Text>
                        </View>
                        <SvgUri style={styles.arrowBtn} width={15} source={require('../../../assets/icon/profil/arrow-right.svg')} />
                    </TouchableOpacity>

                    <TouchableOpacity activeOpacity={1} onPress={() => this.handlerView("Login", { isLoading: false })} style={styles.logoutBlock}>
                        <Text style={styles.logout}>Déconnexion</Text>
                    </TouchableOpacity>
                
                </View>
            </View>
        )
    }
}