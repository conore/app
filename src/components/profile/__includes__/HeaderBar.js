import React from 'react'
import { View, Text, Platform, TouchableOpacity } from 'react-native'
import SvgUri from 'expo-svg-uri'

import styles from '../../../styles/ListClubs'

function backBtn(props) {
    props.backBtn()
}

export default function HeaderBar(props) {

    if(Platform.OS === 'ios') {
        return (
            <View style={styles.topBar}>
                <View style={styles.alignTopBar}>
                    <TouchableOpacity activeOpacity={1} onPress={() => backBtn(props)} style={{ flex: 1, paddingRight: 10 }}>
                        <SvgUri width={20} height={60} style={styles.backBtn} source={require('../../../assets/icon/date/arrow-date-left.svg')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, paddingRight: 270 }}>
                        <Text style={styles.topBarTxt}>{props.title}</Text>
                    </View>
                    <View
                        style={{ flex: 1, paddingRight: 10 }}>
                    </View>
                </View>
            </View>
        )
    }
    else {
        return (
            <View style={styles.topBar}>
                <View style={styles.alignTopBar}>
                    <TouchableOpacity activeOpacity={1} onPress={() => backBtn(props)}>
                        <SvgUri width={20} height={60} style={styles.backBtn} source={require('../../../assets/icon/date/arrow-date-left.svg')} />
                    </TouchableOpacity>
                    <Text style={styles.topBarTxt}>{props.title}</Text>
                </View>
            </View>
        )
    }
    
}
