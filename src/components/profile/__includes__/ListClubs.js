import React, { Component } from 'react'
import { Text, View, SafeAreaView, StatusBar, TouchableOpacity, FlatList } from 'react-native'
import ApiRequest from '../../../api/ApiRequest'
import { connect } from 'react-redux'

import LoadingView from '../../utils/LoadingView'

import styles from '../../../styles/ListClubs'
import HeaderBar from './HeaderBar'

class ListClubs extends Component {

    constructor(props) {
        super(props)
        this.state = {
            clubsId: [],
            clubsName: [],
            isLoading: true,
            isFetching: false
        }
    }

    _toogleSwitchClubs(clubId) {
        const action = { type: 'TOOGLE_SWITCH_CLUB', value: clubId }
        this.props.dispatch(action)
    }

    backBtn() {
        this.props.navigation.goBack()
    }

    componentDidMount() {
        this.getClubs()
    }

    componentDidUpdate() {
        this.getClubs()
    }

    onRefresh() {
        this.setState({ 
                isFetching: true 
            },
            function() { 
                this.getClubs()
            }
        );
    }

    getClubs() {
        ApiRequest.getAccount().then(res => {
            if(res.data.status == 'success') {
                this.getClubsName(res.data.data.clubs)
            }
        })
    }

    async getClubsName(clubs) {
        let clubsId = []
        let clubsName = []
        let currentClubId = this.props.clubId

        for(let i = 0; i < clubs.length; i++) {
            await ApiRequest.getOneClub(clubs[i].clubId).then(res => {
                if(res.data.status == 'success') {
                    if(res.data.data.id == currentClubId) {
                        clubsId.unshift(clubs[i].clubId)
                        clubsName.unshift(res.data.data.name)
                    }
                    else {
                        clubsId.push(clubs[i].clubId)
                        clubsName.push(res.data.data.name)
                    }
                }
            })
        }
        this.setState({ currentClubId: currentClubId, clubsId: clubsId, clubsName: clubsName, isLoading: false, isFetching: false })
    }

    isSelected(index) {
        if(this.state.currentClubId == this.state.clubsId[index]) {
            return <Text style={styles.titleClubName}>{this.state.clubsName[index]}</Text>
        }
        else {
            return (
                <TouchableOpacity activeOpacity={1} onPress={() => this._toogleSwitchClubs(this.state.clubsId[index])}>
                    <Text style={styles.titleClubName}>{this.state.clubsName[index]}</Text>
                </TouchableOpacity>
            )
        }
    }

    render() {
        const { isLoading, isFetching, clubsName } = this.state
        return(
            <SafeAreaView style={styles.viewProfil}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <View style={styles.viewProfil}>
                    <HeaderBar title="Vos clubs" backBtn={this.backBtn.bind(this)}/>

                    <LoadingView isLoading={isLoading}/>

                    <FlatList 
                        contentContainerStyle={{ paddingBottom: 10 }}
                        data={clubsName}
                        onRefresh={() => this.onRefresh()}
                        refreshing={isFetching}
                        renderItem={({item, index}) => {
                            if(index == 0) {
                                return (
                                    <View>
                                        <View style={styles.clubNameBlock}>
                                            <Text style={styles.titleClub}>Club selectionné</Text>
                                        </View>
                                        {this.isSelected(index)}
                                        <View style={styles.clubNameBlock}>
                                            <Text style={styles.titleClub}>Vos autres clubs</Text>
                                        </View>
                                    </View>
                                )
                            }
                            else {
                                return this.isSelected(index)
                            }
                        }}
                        keyExtractor={(item,index)=>index}
                    />

                </View>
            </SafeAreaView>
        )
    }

}

const mapStateToProps = state => {
    return {
        clubId: state.clubId
    }
}
export default connect(mapStateToProps)(ListClubs)