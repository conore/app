import React from 'react';
import { View, Text } from 'react-native'
import SvgUri from 'expo-svg-uri'

import styles from '../../../styles/HeaderProfil'

function checkName(email, firstname, lastname) {
    return(
        <Text style={styles.titleProfile}>{(firstname.length > 0 && lastname.length > 0) ? `${firstname} ${lastname}` : email}</Text>
    )
}

export default function HeaderProfile(props) {

    const { email, firstname, lastname } = props

    return (
        <View style={styles.view}>
            <View style={styles.titleBlock}>
                <View style={styles.profileBlock}>
                    <SvgUri width={100} style={{alignItems: 'center'}} source={require('../../../assets/icon/profile/default-profile.svg')} />
                </View>
                {checkName(email, firstname, lastname)}
            </View>
        </View>
    )
}
