import React, { Component } from 'react'
import { Text, View, SafeAreaView, StatusBar, TouchableOpacity, FlatList } from 'react-native'
import ApiRequest from '../../../api/ApiRequest'
import moment from 'moment'

import ItemReservation from '../../utils/ItemReservation'
import LoadingView from '../../utils/LoadingView'

import styles from '../../../styles/ListReservation'
import HeaderBar from './HeaderBar'

export default class ListReservations extends Component {

    constructor(props) {
        super(props)
        this.state = {
            clubs: [],
            reservations: [],
            selected: [],
            isSelected: false,
            isFetching: false,
            isLoading: true,
        }
    }

    backBtn() {
        this.props.navigation.goBack()
    }

    componentDidMount() {
        this.getClubsId()
    }

    _showReservationBtn() {
        if(this.state.isSelected) {
            return(
                <TouchableOpacity style={styles.annulationBtn} onPress={() => this.annulerClick()}>
                    <Text style={styles.reservationTxt}>Annuler la réservation</Text>
                </TouchableOpacity>
            )
        }
    }

    onSelectedReservation(sessionId, index) {
        let selectedTmp = []
        let isSelectedTmp = false
        for(let i = 0; i < this.state.selected.length; i++) {
            if(i === index) {
                this.state.selected[i] ? selectedTmp.push(false) : selectedTmp.push(true)
            }
            else {
                selectedTmp.push(false)
            }
        }
        if(selectedTmp.includes(true)) {
            isSelectedTmp = true
        }

        this.setState({ selected: selectedTmp, isSelected: isSelectedTmp, sessionId: sessionId, clubsIdSelected: index })
    }

    annulerClick() {
        this.setState({ isLoading: true, reservations: [] })
        ApiRequest.removeReservation(this.state.clubsIds[this.state.clubsIdSelected], this.state.sessionId).then(res => {
            this.getClubsId()
        })
        .catch(error => {
            console.log("error",error)
        })
    }

    getClubsId() {
        ApiRequest.getAccount().then(res => {
            if(res.data.status == 'success') {
                this.getReservations(res.data.data.clubs)
            }
        })
    }

    async getReservations(clubs) {
        let reservations = []
        let clubsIds = []
        let clubsName = []

        for(let i = 0; i < clubs.length; i++) {
            for(let j = 0; j < clubs[i].reservations.length; j++) {
                await ApiRequest.getReservation(clubs[i].clubId, clubs[i].reservations[j]).then(res => {
                    if(res.data.status == 'success') {
                        let result = res.data.data
                        result["clubId"] = clubs[i].clubId
                        clubsIds.push(clubs[i].clubId)
                        reservations.push(result)
                    }
                })
                .catch(error => {
                    console.log("error",error)
                    return;
                })

                let nameClub = ""
                await ApiRequest.getOneClub(clubs[i].clubId).then(res => {
                    if(res.data.status == 'success') {
                        nameClub += res.data.data.name
                    }
                })
                .catch(error => {
                    console.log("error",error)
                    return;
                })

                clubsName.push({ clubId: clubs[i].clubId, clubName: nameClub })
            }

        }
        let selected = []
        for(let i = 0; i < reservations.length; i++) {
            selected.push(false)
        }

        // sort by event_date
        reservations.sort(function(a,b) {
            return new Date(a.event_at) - new Date(b.event_at)
        })

        // sort by clubId
        reservations.sort(function(a,b) {
            return new Date(a.clubId) - new Date(b.clubId)
        })
        reservations.sort(function(a,b) {
            return new Date(a.clubId) - new Date(b.clubId)
        })

        clubsName.sort(function(a,b) {
            return new Date(a.clubId) - new Date(b.clubId)
        })

        clubsIds.sort()

        this.setState({
            reservations: reservations,
            clubsIds: clubsIds,
            clubsName: clubsName,
            selected: selected,
            isSelected: false,
            isFetching: false,
            isLoading: false
        })
    }

    onRefresh() {
        this.setState({ 
                isFetching: true 
            },
            function() { 
                this.getClubsId()
            }
        );
    }

    renderItem(today, future, index, item, selected) {
        if(today.diff(future) <= 0) {
            return (
                <TouchableOpacity activeOpacity={1} onPress={() => this.onSelectedReservation(item.id, index)}>
                    <ItemReservation 
                        id={item.id} 
                        hours={moment.unix(item.event_at).format('HH[h]mm')} 
                        duration={item.duration} 
                        hasReservated={item.has_reservated} 
                        selected={selected[index]}
                        alreadyReserved={true}
                        currentDay={new Date((item.event_at*1000)-3600)}
                    />
                </TouchableOpacity>
            )
        }
    }

    render() {
        const { reservations, isLoading, isFetching, selected, clubsIds, clubsName } = this.state
        if(reservations.length > 0) {
            return (
                <SafeAreaView style={styles.viewProfil}>
                    <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                    <View style={styles.viewProfil}>
                        <HeaderBar title="Vos réservations" backBtn={this.backBtn.bind(this)} />

                        <LoadingView isLoading={isLoading}/>

                        <FlatList 
                            contentContainerStyle={{ paddingBottom: 10 }}
                            data={reservations}
                            onRefresh={() => this.onRefresh()}
                            refreshing={isFetching}
                            renderItem={({item, index}) => {
                                let future = moment.unix(item.event_at).add(1, 'hours')
                                let today = moment().add(1, 'hours')
                                if(index == 0) {
                                    return (
                                        <View>
                                            <View style={styles.clubNameBlock}>
                                                <Text style={styles.titleClub}>Clubs {clubsName[index].clubName}</Text>
                                            </View>
                                            {this.renderItem(today, future, index, item, selected)}
                                        </View>
                                    )
                                }
                                if(clubsIds[index] == clubsIds[index-1]) {
                                    return this.renderItem(today, future, index, item, selected)
                                }
                                if(clubsIds[index] != clubsIds[index-1]) {
                                    return (
                                        <View>
                                            <View style={styles.clubNameBlock}>
                                                <Text style={styles.titleClub}>Clubs {clubsName[index].clubName}</Text>
                                            </View>
                                            {this.renderItem(today, future, index, item, selected)}
                                        </View>
                                    )
                                }
                            }}
                            keyExtractor={(item,index)=>index}
                        />
                        {this._showReservationBtn()}
                    </View>
                </SafeAreaView>
            )
        }
        else {
            return (
                <SafeAreaView style={styles.viewProfil}>
                    <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                    <View style={styles.viewProfil}>
                        <HeaderBar title="Vos réservations" backBtn={this.backBtn.bind(this)} />
                        <LoadingView isLoading={isLoading}/>
                    </View>
                </SafeAreaView>
            )
        }
    }

}
