import React, { Component } from 'react'
import { Text, View, SafeAreaView, StatusBar, ScrollView, TouchableOpacity } from 'react-native'
import SvgUri from 'expo-svg-uri'
import ApiRequest from '../../api/ApiRequest'
import { userAuth, userLogin } from '../../api/__includes__/authentication'
import m from '../utils/messages'

import HeaderBar from '../utils/HeaderBar'
import HeaderProfile from './__includes__/HeaderProfile'
import InfoProfle from './__includes__/InfoProfle'

import styles from '../../styles/Profil'

export default class Profil extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id: null,
            username: "",
            email: "",
            firstname: "",
            lastname: "",
            clubs: [],
            errorNetwork: false,
            errorMsg: ""
        }
        this.handlerView = this.handlerView.bind(this)
    }

    componentDidMount() {
        ApiRequest.getAccount().then(res => {
            if(res.data.status == 'success') {
                this.setState({
                    id: res.data.data.id,
                    username: res.data.data.username,
                    email: res.data.data.email,
                    firstname: res.data.data.firstname,
                    lastname: res.data.data.lastname,
                    clubs: res.data.data.clubs,
                    errorNetwork: false,
                    errorMsg: ""
                })
            }
        })
        .catch(error => {
            console.log("error",error)
            this.setState({
                errorNetwork: true,
                errorMsg: m('general/error-network')
            })
            return;
        })
    }

    handlerView(view, data) {
        const { navigation } = this.props
        if(view == "Login") {
            ApiRequest.logout().then(async res => {
                await userAuth("", "")
                await userLogin("", "")
            })
        }
        navigation.navigate(view, data)
    }

    _showAccount() {
        const { email, firstname, lastname, clubs, errorNetwork, errorMsg } = this.state
        if(errorNetwork) {
           return (
               <View style={styles.errorNetworkContainer}>
                    <View>
                        <SvgUri width={60} style={{alignItems: 'center'}} source={require('../../assets/icon/errors/icon-warning.svg')} />
                        <Text style={styles.errorMsg}>{errorMsg}</Text>
                    </View>
               </View>
           )
        }
        else {
            return (
                <View style={{flex: 1}}>
                    <HeaderProfile email={email} firstname={firstname} lastname={lastname} />
                    <InfoProfle clubs={clubs} handlerView={this.handlerView} />
                </View>
            )
        }
    }
    

    render() {
        return (
            <SafeAreaView style={styles.viewProfil}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <View style={styles.viewProfil}>
                   <HeaderBar title="Profil" />

                    <ScrollView style={styles.scrollView}>
                        {this._showAccount()}
                    </ScrollView>

                </View>
            </SafeAreaView>
        )
    }
}
