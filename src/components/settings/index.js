import React, { Component } from 'react'
import { StyleSheet, Text, View, SafeAreaView, StatusBar, TouchableOpacity } from 'react-native'
import SvgUri from 'expo-svg-uri'
import HeaderBar from '../profile/__includes__/HeaderBar'

export default class Settings extends Component {

    constructor(props) {
        super(props)
    }

    backBtn() {
        this.props.navigation.goBack()
    }

    logoutClick() {
        this.props.navigation.navigate("Login")
    }

    render() {
        return (
            <SafeAreaView style={styles.viewProfil}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <View style={styles.viewProfil}>
                    <HeaderBar title="Paramètres" backBtn={this.backBtn.bind(this)} />



                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    viewProfil:{ 
        flex: 1,
    },
    topBar: {
        height: 60,
        backgroundColor: '#fff',
        borderBottomWidth: 0.5,
        borderBottomColor: '#d6d6d6',
        shadowColor: '#d6d6d65e',
        elevation: 2,
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 2,
        shadowOpacity: 1.0
    },
    topBarTxt:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#5e72e4',
        marginLeft: 15
    },
    alignTopBar: {
        flexDirection: "row",
        alignItems: "center"
    },
    backBtn: {
        marginLeft: 15
    },
})