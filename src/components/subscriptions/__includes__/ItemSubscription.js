import React from 'react';
import { Text, View } from 'react-native';
import styles from '../../../styles/ItemSubscription'


function frequencyFormat(frequency) {
    if(frequency == "day") {
        return "jour"
    }
    if(frequency == "week") {
        return "semaine"
    }
    if(frequency == "month") {
        return "mois"
    }
    if(frequency == "year") {
        return "an"
    }
}

function expireFormat(days, months, years) {
    let expire = ""
    if(days <= 31) {
        expire = (days <= 1) ? `${days} jour` : `${days} jours` 
        if(days <= 3) {
            return (
                <View style={styles.subTitleBlockAlert}>
                    <Text style={styles.subTitleAlert}>{`expire dans ${expire}`}</Text>
                </View>
            ) 
        }
        if(days <= 7) {
            return (
                <View style={styles.subTitleBlockWarning}>
                    <Text style={styles.subTitleWarning}>{`expire dans ${expire}`}</Text>
                </View>
            ) 
        }
        else {
            return (
                <View style={styles.subTitleBlock}>
                    <Text style={styles.subTitle}>{`expire dans ${expire}`}</Text>
                </View>
            ) 
        }
    }
    else if(months <= 12) {
        expire = `${months} mois`
    }
    else if(years <= 12) {
        expire = (years <= 1) ? `${years} an` : `${years} ans` 
    }
    return (
        <View style={styles.subTitleBlock}>
            <Text style={styles.subTitle}>{`expire dans ${expire}`}</Text>
        </View>
    ) 
}

export default function ItemSubscription(props) {

    const { subscription, days, months, years} = props

    if(days > 0) {
        return (
            <View style={styles.abonnementDetails}>
                <View>
                    <Text style={styles.title}>{subscription.name}</Text>
                    <Text style={styles.price}>{`${subscription.amount}€/${frequencyFormat(subscription.frequency)}`}</Text>
                </View>
    
                <View style={styles.subIcon}>
                    {expireFormat(days, months, years)}
                </View>
            </View>
        )
    }
    else {
        return (
            <View style={styles.abonnementDetailsEmpty}>
                <View>
                    <Text style={styles.emptySubTxt}>Aucun abonnement</Text>
                </View>
            </View>
        )
    }
}