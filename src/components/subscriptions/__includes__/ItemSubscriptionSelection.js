import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import SvgUri from 'expo-svg-uri';


function frequencyFormat(frequency) {
    if(frequency == "day") {
        return "jour"
    }
    if(frequency == "week") {
        return "semaine"
    }
    if(frequency == "month") {
        return "mois"
    }
    if(frequency == "year") {
        return "an"
    }
}

function checkSelected(isSelected) {
    if(isSelected) {
        return (
            <View>
                <SvgUri width={20} style={{alignItems: 'center'}} source={require('../../../assets/icon/payment/selectBtnActive.svg')} />
            </View>
        )
    }
    else {
        return (
            <View>
                <SvgUri width={20} style={{alignItems: 'center'}} source={require('../../../assets/icon/payment/selectBtn.svg')} />
            </View>
        )
    }
}

export default function ItemSubscriptionSelection(props) {

    const { subscription, isSelected } = props

    return (
        <View style={styles.abonnementSelect}>
            <View>
                <Text style={styles.title}>{subscription.name}</Text>
                <Text style={styles.price}>{`${subscription.amount}€/${frequencyFormat(subscription.interval)}`}</Text>
            </View>

            {checkSelected(isSelected)}
        </View>
    )
}

const styles = StyleSheet.create({
    abonnementSelect: {
        paddingLeft: 35,
        paddingRight: 35,
        paddingTop: 15,
        paddingBottom: 15,
        borderTopWidth: 1,
        borderColor: "#e8e8e8",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
    },
    price: {
        color: '#ffb016',
        fontSize: 17
    },
})