import React, { Component } from 'react'
import { Text, View, SafeAreaView, StatusBar, FlatList, TouchableOpacity, BackHandler, ScrollView } from 'react-native'
import ApiRequest from '../../../api/ApiRequest'
import SvgUri from 'expo-svg-uri'
import LoadingView from '../../utils/LoadingView'
import ItemSubscriptionSelection from './ItemSubscriptionSelection'
import styles from '../../../styles/SubscriptionSelection'
import { userAuth, userLogin } from '../../../api/__includes__/authentication'

export default class SubscriptionSelection extends Component {

    constructor(props) {
        super(props)
        this.state = {
            subscriptions: [],
            clubIds: props.navigation.state.params.clubIds,
            displayBtn: false,
            clubsName: [],
            isLoading: true
        }
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.navigate("Login")
        })
        this.getSubscriptions()
    }
    
    onClickPay() {
        let { subscriptionsSelected, subscriptions } = this.state
        let selections = []
        let totalPrice = 0

        for (let i = 0; i < subscriptionsSelected.length; i++) {
            for (let j = 0; j < subscriptionsSelected[i].length; j++) {
                if(subscriptionsSelected[i][j]){
                    selections.push(subscriptions[i][j])
                    totalPrice += subscriptions[i][j].amount
                }
            }
        }

        if(selections.length > 0) {
            this.props.navigation.navigate("Payments", { subscriptions: selections, totalPrice: totalPrice.toFixed(2), navigate: 'Home' })
        }
    }

    frequencyFormat(frequency) {
        if(frequency == "day") {
            return "jour"
        }
        if(frequency == "week") {
            return "semaine"
        }
        if(frequency == "month") {
            return "mois"
        }
        if(frequency == "year") {
            return "an"
        }
    }

    async getSubscriptions() {
        let { clubIds } = this.state

        let subscriptions = []
        let subscriptionsSelected = []
        let clubsName = []

        for(let i = 0; i < clubIds.length; i++) {
            let clubId = clubIds[i]

            let clubName = ""
            await ApiRequest.getOneClub(clubId).then(async res => {

                if(res.data.status == 'success') {
                    clubName = res.data.data.name
                    clubsName.push(clubName)
                }
            })
            .catch(err => {
                console.log(err)
            })

            await ApiRequest.getClubSubscriptions(clubId).then(res => {

                if(res.data.status == 'success') {
                    let subscription = res.data.data
                    let seletedArray = []

                    for(let j = 0; j < subscription.length; j++) {
                        subscription[j]["clubId"] = clubId
                        subscription[j]["clubName"] = clubName
                        seletedArray.push(false)
                    }
                    subscriptions.push(subscription)
                    subscriptionsSelected.push(seletedArray)
                }
            }).catch(err => {
                console.log(err)
            })
        }
        
        this.setState({
            subscriptions: subscriptions,
            subscriptionsSelected: subscriptionsSelected,
            clubsName: clubsName,
            isLoading: false
        })
    }

    changeSelection(state, index, index2) {
        let { subscriptionsSelected } = this.state
        let subscriptionsSelectedTmp = subscriptionsSelected
        let displayBtn = false

        for (let i = 0; i < subscriptionsSelectedTmp.length; i++) {
            for (let j = 0; j < subscriptionsSelectedTmp[i].length; j++) {
                if(i == index && j == index2) {
                    subscriptionsSelectedTmp[i][j] = state
                }
                else if(i == index && j != index2) {
                    subscriptionsSelectedTmp[i][j] = false
                }
                if(subscriptionsSelectedTmp[i][j]) {
                    displayBtn = true
                }
            }
            
        }
        this.setState({ subscriptionsSelected: subscriptionsSelectedTmp, displayBtn: displayBtn })
    }

    backLogin() {
        ApiRequest.logout().then(async res => {
            await userAuth("", "")
            await userLogin("", "")
        })
        this.props.navigation.navigate("Login", { isLoading: false })
    }

    renderItem(i, item, subscriptionsSelected) {
        let subLength = this.state.subscriptions.length
        let subLength2 = subscriptionsSelected.length
        if(item.length > 0) {
            return item.map((subscription, index) => {
                if(i == subLength-1 && index == subLength2-1) {
                    if(subscriptionsSelected[index]) {
                        return (
                            <TouchableOpacity activeOpacity={1} style={{ borderBottomWidth: 1, borderColor: "#e8e8e8"}} onPress={() => this.changeSelection(false, i, index)}>
                                <ItemSubscriptionSelection subscription={subscription} isSelected={true} /> 
                            </TouchableOpacity>
                        )
                    }
                    else {
                        return (
                            <TouchableOpacity activeOpacity={1} style={{ borderBottomWidth: 1, borderColor: "#e8e8e8"}} onPress={() => this.changeSelection(true, i, index)}>
                                <ItemSubscriptionSelection subscription={subscription} isSelected={false} /> 
                            </TouchableOpacity>
                        )
                    }
                }
                else {
                    if(subscriptionsSelected[index]) {
                        return (
                            <TouchableOpacity activeOpacity={1} onPress={() => this.changeSelection(false, i, index)}>
                                <ItemSubscriptionSelection subscription={subscription} isSelected={true} /> 
                            </TouchableOpacity>
                        )
                    }
                    else {
                        return (
                            <TouchableOpacity activeOpacity={1} onPress={() => this.changeSelection(true, i, index)}>
                                <ItemSubscriptionSelection subscription={subscription} isSelected={false} /> 
                            </TouchableOpacity>
                        )
                    }
                }
            })
        }
    }

    displayConfirmBtn() {
        let { displayBtn } = this.state

        if(displayBtn) {
            return (
                <View>
                    <TouchableOpacity activeOpacity={1} onPress={() => this.onClickPay()} style={styles.confirmBtnActive}>
                        <Text style={styles.confirmBtnTxtActive}>Valider</Text>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={1} onPress={() => this.backLogin()}>
                        <Text style={styles.backLoginTxt}>Retour</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        else {
            return (
                <View>
                    <View style={styles.confirmBtn}>
                        <Text style={styles.confirmBtnTxt}>Valider</Text>
                    </View>
                    <TouchableOpacity activeOpacity={1} onPress={() => this.backLogin()}>
                        <Text style={styles.backLoginTxt}>Retour</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    render() {
        const { subscriptions, subscriptionsSelected, isLoading, clubIds, clubsName } = this.state
        
        if(subscriptions.length > 0) {
            return(
                <SafeAreaView>
                    <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                    <LoadingView isLoading={isLoading}/>
                    
                    <ScrollView>
                        <View>
                            <SvgUri width={150} style={{alignItems: 'center', marginTop: 40}} source={require('../../../assets/icon/profile/yoga2.svg')} />
                            <Text style={styles.title}>Vous y êtes presque !</Text>
                            <Text style={styles.subtitle}>Veuillez vous abonner à au moins un de vos clubs</Text>
                        </View>

                        <FlatList 
                            contentContainerStyle={{ paddingBottom: 10 }}
                            data={subscriptions}
                            renderItem={({item, index}) => {
                                if(index == 0) {
                                    return (
                                        <View>
                                            <Text style={styles.titleClub}>{clubsName[index]}</Text>
                                            {this.renderItem(index, item, subscriptionsSelected[index])}
                                        </View>
                                    )
                                }
                                if(clubIds[index] == clubIds[index-1]) {
                                    return this.renderItem(index, item, subscriptionsSelected[index])
                                }
                                if(clubIds[index] != clubIds[index-1]) {
                                    return (
                                        <View style={styles.otherClub}>
                                            <Text style={styles.titleClub}>{clubsName[index]}</Text>
                                            {this.renderItem(index, item, subscriptionsSelected[index])}
                                        </View>
                                    )
                                }
                            }}
                            keyExtractor={(item,index)=>index}
                        />

                        {this.displayConfirmBtn()}
                    </ScrollView>
                </SafeAreaView>
            )
        }
        else {
            return(
                <SafeAreaView style={{flex: 1}}>
                    <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                    <LoadingView isLoading={isLoading}/>
                </SafeAreaView>
            )
        }
    }
}