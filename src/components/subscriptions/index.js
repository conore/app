import React, { Component } from 'react'
import { Text, View, SafeAreaView, StatusBar, FlatList } from 'react-native'
import ApiRequest from '../../api/ApiRequest'
import moment from 'moment'
import LoadingView from '../utils/LoadingView'

import styles from '../../styles/ListReservation'
import HeaderBar from '../profile/__includes__/HeaderBar'
import ItemSubscription from './__includes__/ItemSubscription'

export default class Subscriptions extends Component {

    constructor(props) {
        super(props)
        this.state = {
            subscriptions: [],
            clubsIds: [],
            clubsName: [],
            isFetching: false,
            isLoading: true,
        }
    }

    backBtn() {
        this.props.navigation.goBack()
    }

    componentDidMount() {
        this.getSubscriptions()
    }

    getSubscriptions() {
        ApiRequest.getAccount().then( async res => {
            if(res.data.status == 'success') {
                let clubs = res.data.data.clubs
                let clubsIds = []
                let clubsName = []
                let subscriptions = []

                for(let i = 0; i < clubs.length; i++) {
                    let subscription = clubs[i].subscriptions[0]

                    if(clubs[i].subscriptions.length > 0) {
                        if(subscription.name != undefined) {
                            subscription["clubId"] = clubs[i].clubId
                            clubsIds.push(clubs[i].clubId)
                            subscriptions.push(subscription)
                        }
                    }

                    let nameClub = ""
                    await ApiRequest.getOneClub(clubs[i].clubId).then(res => {
                        if(res.data.status == 'success') {
                            nameClub += res.data.data.name
                        }
                    })
                    .catch(error => {
                        console.log("error",error)
                        return;
                    })
                    clubsName.push({ clubId: clubs[i].clubId, clubName: nameClub })
                }

                // sort by clubId
                subscriptions.sort(function(a,b) {
                    return new Date(a.clubId) - new Date(b.clubId)
                })

                clubsIds.sort()
                
                this.setState({
                    subscriptions: subscriptions,
                    clubsIds: clubsIds,
                    clubsName: clubsName,
                    isFetching: false,
                    isLoading: false
                })
            }
        })
    }
       
    onRefresh() {
        this.setState({ 
                isFetching: true 
            },
            function() { 
                this.getSubscriptions()
            }
        );
    }

    renderItem(index, item) {
        let { clubsIds } = this.state

        let end_date = moment.unix(item.end_date)
        let start_date = moment()
        let days = end_date.diff(start_date,'days')
        let months = end_date.diff(start_date,'months')
        let years = end_date.diff(start_date,'years')

        if(item.clubId == clubsIds[index]) {
            return (
                <View>
                    <ItemSubscription subscription={item} days={days} months={months} years={years}/>
                </View>
            )
        }
    }

    render() {
        const { subscriptions, isLoading, isFetching, clubsIds, clubsName } = this.state
        if(subscriptions.length > 0) {
            return (
                <SafeAreaView style={styles.viewProfil}>
                    <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                    <View style={styles.viewProfil}>
                        <HeaderBar title="Vos abonnements" backBtn={this.backBtn.bind(this)} />

                        <LoadingView isLoading={isLoading}/>

                        <FlatList 
                            contentContainerStyle={{ paddingBottom: 10 }}
                            data={subscriptions}
                            onRefresh={() => this.onRefresh()}
                            refreshing={isFetching}
                            renderItem={({item, index}) => {
                                if(index == 0) {
                                    return (
                                        <View>
                                            <View style={styles.clubNameBlock}>
                                                <Text style={styles.titleClub}>Club {clubsName[index].clubName}</Text>
                                            </View>
                                            {this.renderItem(index, item)}
                                        </View>
                                    )
                                }
                                if(clubsIds[index] == clubsIds[index-1]) {
                                    return this.renderItem(index, item)
                                }
                                if(clubsIds[index] != clubsIds[index-1]) {
                                    return (
                                        <View>
                                            <View style={styles.clubNameBlock}>
                                                <Text style={styles.titleClub}>Club {clubsName[index].clubName}</Text>
                                            </View>
                                            {this.renderItem(index, item)}
                                        </View>
                                    )
                                }
                            }}
                            keyExtractor={(item,index)=>index}
                        />
                    </View>
                </SafeAreaView>
            )
        }
        else {
            return (
                <SafeAreaView style={styles.viewProfil}>
                    <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                    <View style={styles.viewProfil}>
                        <HeaderBar title="Vos abonnements" backBtn={this.backBtn.bind(this)} />
                        <LoadingView isLoading={isLoading}/>
                    </View>
                </SafeAreaView>
            )
        }
    }

}
