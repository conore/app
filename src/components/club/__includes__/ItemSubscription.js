import React from 'react';
import { Text, View } from 'react-native';
import styles from '../../../styles/ItemSubscription'


function frequencyFormat(frequency) {
    if(frequency == "day") {
        return "jour"
    }
    if(frequency == "week") {
        return "semaine"
    }
    if(frequency == "month") {
        return "mois"
    }
    if(frequency == "year") {
        return "an"
    }
}

export default function ItemSubscription(props) {

    const { subscription, active } = props

    if(active == null) {
        return (
            <View style={styles.abonnementEmpty}>
                <View>
                    <Text style={styles.emptySubTxt}>Aucun abonnement proposé</Text>
                </View>
            </View>
        )
    }
    else if(!active) {
        return (
            <View style={styles.abonnement}>
                <View>
                    <Text style={styles.title}>{subscription.name}</Text>
                    <Text style={styles.price}>{`${subscription.amount}€/${frequencyFormat(subscription.interval)}`}</Text>
                </View>
    
                <View style={styles.subIcon}>
                    <View style={styles.subTitleBlock}>
                        <Text style={styles.subTitle}>S'abonner</Text>
                    </View>
                </View>
            </View>
        )
    }
    else if(active){
        return (
            <View style={styles.abonnementActive}>
                <View>
                    <Text style={styles.title}>{subscription.name}</Text>
                    <Text style={styles.priceActive}>{`${subscription.amount}€/${frequencyFormat(subscription.interval)}`}</Text>
                </View>
    
                <View style={styles.subIcon}>
                    <View style={styles.subTitleBlockActive}>
                        <Text style={styles.subTitleActive}>Votre abonnement</Text>
                    </View>
                </View>
            </View>
        )
    }
}