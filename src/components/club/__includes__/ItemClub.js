import React from 'react'
import { Text, View, Linking, TouchableOpacity, Platform} from 'react-native'
import SvgUri from 'expo-svg-uri'
import styles from '../../../styles/ItemClub'

function openMaps(address) {
    Linking.openURL(`https://www.google.com/maps/search/?api=1&query=${address}`)
}

function openTel(number) {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
        phoneNumber = `tel:${number}`
    }
    else {
        phoneNumber = `telprompt:${number}`
    }
 
    Linking.openURL(phoneNumber)
}

export default function ItemClub(props) {

    const { name, tel, address, zipCode, city } = props
    return (
        <View style={styles.view}>
            
            <View style={styles.titleBlock}>
                 {/* Title */}
                <SvgUri width={70} style={{alignItems: 'center', marginTop: 30}} source={require('../../../assets/icon/navbar/icon-clubs-view.svg')} />
                <Text style={styles.title}>{name}</Text>
                
                {/* Maps btn */}
                <TouchableOpacity style={styles.addressBlock} activeOpacity={1} onPress={() => openMaps(`${address} ${zipCode} ${city}`)}>
                    <View style={styles.mapBlock}>
                        <SvgUri width={50} style={{alignItems: 'center', marginLeft: 0}} source={require('../../../assets/icon/navbar/icon-maps.svg')} />
                    </View>
                    <View>
                        <Text style={styles.address}>{address}</Text>
                        <Text style={styles.city}>{zipCode} {city}</Text>
                    </View>
                </TouchableOpacity>

                {/* Phone btn */}
                <TouchableOpacity style={styles.telBlock} activeOpacity={1} onPress={() => openTel(tel)}>
                    <View style={styles.telLeft}>
                        <SvgUri width={50} style={{alignItems: 'center', marginLeft: 20}} source={require('../../../assets/icon/navbar/icon-tel.svg')} />
                    </View>

                    <View style={styles.blockTel}>
                        <Text style={styles.tel}>{tel}</Text>
                    </View> 
                </TouchableOpacity>
            </View>

        </View>
    )
}