import React, { Component } from 'react'
import { View, SafeAreaView, StatusBar, ScrollView, Text } from 'react-native'
import ApiRequest from '../../api/ApiRequest'
import { connect } from 'react-redux'

import ItemClub from './__includes__/ItemClub'
import ItemSubscription from './__includes__/ItemSubscription'
import ItemNoResult from '../utils/ItemNoResult'
import LoadingView from '../utils/LoadingView'
import HeaderBar from '../utils/HeaderBar'

import styles from '../../styles/Clubs'

class Club extends Component {

    constructor(props) {
        super(props)
        this.state = {
            clubId: null,
            isLoading: true,
            noResult: false,
            isFetching: false,
            subscriptions: [],
            userSubscription: {},
            clubs: null,
            errorNetwork: false,
            errorMsg: null
        }
    }

    componentDidMount() {
        this._getClub()
    }

    componentDidUpdate() {
        this._getClub()
    }

    _getClub() {
        let clubId = this.props.clubId
        if(clubId !== this.state.clubId) {
            ApiRequest.getOneClub(clubId).then(res => {

                if(res.data.status == 'success') {
                    let result = res.data.data
                    
                    if(result.length == 0) {
                        this.setState({
                            clubId: result.id,
                            clubs: null,
                            isLoading: false,
                            noResult: true,
                            isFetching: false
                        })
                    }
                    else {
                        this.setState({
                            clubId: result.id,
                            clubs: result,
                            isLoading: false,
                            noResult: false,
                            isFetching: false
                        })
                    }
                }
            })
            .catch(error => {
                console.log("error",error)
                this.setState({ errorNetwork: true })
                return;
            })

            ApiRequest.getClubSubscriptions(clubId).then(res => {
                if(res.data.status == 'success') {
                    this.setState({ subscriptions: res.data.data })
                }
            })

            ApiRequest.getAccount().then(res => {
                if(res.data.status == 'success') {
                    for(let i = 0; i < res.data.data.clubs.length; i++) {
                        if(this.state.clubId == res.data.data.clubs[i].clubId) {
                            if(res.data.data.clubs[i].subscriptions.length > 0) {
                                this.setState({ userSubscription: res.data.data.clubs[i].subscriptions[0] })
                            }
                        }
                    }
                }
            })
        }
    }

    _displayLoading() {
        let { isLoading } = this.state
        if(isLoading) {
            return (
                <LoadingView isLoading={isLoading} />
            )
        }
    }

    _showNoClubs() {
        if(this.state.noResult) {
            return (
                <View style={styles.noClubs}>
                    <ItemNoResult msg="Aucun clubs"/>
                </View>
            )
        }
    }

    displaySubscriptions() {
        let { subscriptions, userSubscription } = this.state

        if(subscriptions.length > 0) {
            return subscriptions.map((subscription) => {
                if(subscription.name === userSubscription.name) {
                    return <ItemSubscription subscription={subscription} active={true} />
                }
                else {
                    return <ItemSubscription subscription={subscription} active={false} />
                }
            })
        }
        else {
            return <ItemSubscription />
        }
    }

    _displayClubs() {
        let { clubs } = this.state
        if(clubs != null) {
            return (
                <View style={styles.containerClubs}>
                    <ItemClub name={clubs.name} tel={clubs.phone} address={clubs.address} zipCode={clubs.zip_code} city={clubs.city} />
                    <Text style={styles.abonnementTitle}>Abonnements</Text>
                    {this.displaySubscriptions()}
                </View>
            )
        }
    }

    activeBtn() {
        this.props.navigation.navigate("ListClubs")
    }

    render() {
        return (
            <SafeAreaView style={styles.viewClubs}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <View style={styles.viewClubs}>
                    <HeaderBar title="Club" activeBtn={this.activeBtn.bind(this)} />
                    {this._displayLoading()}
                    <ScrollView>
                        {this._displayClubs()}
                        {this._showNoClubs()}
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
    
}

const mapStateToProps = state => {
    return {
        clubId: state.clubId
    }
}
export default connect(mapStateToProps)(Club)