import React, { Component } from 'react'
import { View, BackHandler, SafeAreaView, StatusBar } from 'react-native'
import ApiRequest from '../../api/ApiRequest'
import { connect } from 'react-redux'
import moment from 'moment'

import CalendarBar from './__includes__/CalendarBar'
import ListReservation from './__includes__/ListReservation'
import LoadingView from '../utils/LoadingView'
import HeaderBar from '../utils/HeaderBar'
import PushNotification from '../../notifications/PushNotification'

import styles from '../../styles/Reserver'

class Reserver extends Component {

    constructor(props) {
        super(props)
        this.state = {
            clubId: props.clubId,
            selected: [],
            isSelected: false,
            isReserved: false,
            reservations: [],
            isFetching: false,
            isLoading: true,
            noResult: false,
            currentDay: moment().format("YYYY-MM-DD")
        }
    }

    _toogleSwitchClubs() {
        const action = { type: 'TOOGLE_SWITCH_CLUB', value: this.state.clubId }
        this.props.dispatch(action)
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
          // Do nothing
          return true;
        })
        this._getSessions()
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    componentDidUpdate(prevProps, prevState) {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            // Do nothing
            return true;
        })
        if(prevState.currentDay !== this.state.currentDay) {
            this._getSessions()
        }

        if(prevProps.clubId !== this.props.clubId) {
            this._getSessions(prevProps.clubId)
        }
    }

    onRefresh() {
        this.setState({ 
                isFetching: true 
            },
            function() { this._getSessions() }
        )
    }

    day(day) {
        this.setState({
            currentDay: day,
            reservations: [],
            isLoading: true,
            noResult: false
        })
    }

    // Selected reservation
    onSelectedReservation(sessionId, places, index, has_reservated) {
        if(places > 0) {
            let selectedTmp = []
            let isSelectedTmp = false
            for(let i = 0; i < this.state.selected.length; i++) {
                if(i === index) {
                    this.state.selected[i] ? selectedTmp.push(false) : selectedTmp.push(true)
                }
                else {
                    selectedTmp.push(false)
                }
            }
            if(selectedTmp.includes(true)) {
                isSelectedTmp = true
            }
            if(has_reservated) {
                this.setState({ selected: selectedTmp, isSelected: false, isReserved: isSelectedTmp, sessionId: sessionId })
            }
            else {
                this.setState({ selected: selectedTmp, isSelected: isSelectedTmp, isReserved: false, sessionId: sessionId })
            }
        }
    }

    reserverClick() {
        this.setState({ isLoading: true, reservations: [] })
        ApiRequest.setReservation(this.props.clubId, this.state.sessionId).then(res => {
            this._getSessions()
        }) 
        .catch(error => {
            console.log("error",error)
        })
    }

    annulerClick() {
        this.setState({ isLoading: true, reservations: [] })
        ApiRequest.removeReservation(this.props.clubId, this.state.sessionId).then(res => {
            this._getSessions()
        }) 
        .catch(error => {
            console.log("error",error)
        })
    }

    activeBtn() {
        this.props.navigation.navigate("ListClubs")
    }

    // Get all session from API
    _getSessions(prevClubId = null) {
        ApiRequest.getAccount().then(async res => {
            if(res.data.status == 'success') {
                let clubId;
                let userId = res.data.data.id

                if(prevClubId == null) {
                    if(this.props.clubId === null) {
                        clubId = res.data.data.clubs[0].clubId
                    }
                    else {
                        clubId = this.props.clubId
                    }
                }
                else {
                    clubId = this.props.clubId
                }

                ApiRequest.getOneClub(clubId).then(res => {
                    if(res.data.status == 'success') {
                        this.setState( {
                            clubName: res.data.data.name
                        })
                    }
                })

                ApiRequest.getSessionClub(clubId, this.state.currentDay, userId).then(res => {
                    if(res.data.status == 'success') {
                        let result = res.data.data

                        if(result.length == 0) {
                            this.setState({
                                reservations: [{id: 0}],
                                selected: [],
                                isFetching: false,
                                isSelected: false,
                                isReserved: false,
                                isLoading: false,
                                noResult: true,
                                clubId: clubId
                            }, function() {
                                this._toogleSwitchClubs()
                            })
                        }
                        else if(result.length >= 1) {
                            let selected = []
                            for(let i = 0; i < result.length; i++) {
                                selected.push(false)
                            }
                            if(this.props.clubId !== clubId) {
                                this.setState({
                                    reservations: result,
                                    selected: selected,
                                    isFetching: false,
                                    isSelected: false,
                                    isReserved: false,
                                    isLoading: false,
                                    noResult: false,
                                    clubId: clubId
                                }, function() {
                                    this._toogleSwitchClubs()
                                })
                            }
                            else {
                                this.setState({
                                    reservations: result,
                                    selected: selected,
                                    isFetching: false,
                                    isSelected: false,
                                    isReserved: false,
                                    isLoading: false,
                                    noResult: false
                                })
                            }
                        }
                    }
                })
                .catch(error => {
                    console.log("error",error)
                    return;
                })
            }
        })
        .catch(error => {
            console.log("error",error)
            return;
        })
    }

    render() {
        const { selected, reservations, isFetching, isLoading, isSelected, isReserved, noResult, clubName} = this.state
        return (
            <SafeAreaView style={styles.viewReserver}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <PushNotification />
                <View style={styles.viewReserver}>
                    <HeaderBar title={`Réserver - ${clubName}`} activeBtn={this.activeBtn.bind(this)}/>

                    <CalendarBar day={this.day.bind(this)}/>
                    <LoadingView isLoading={isLoading}/>
                    
                    <ListReservation 
                        reservations={reservations} 
                        selected={selected} 
                        isFetching={isFetching}
                        handlerSelected={this.onSelectedReservation.bind(this)}
                        handlerRefresh={this.onRefresh.bind(this)}
                        handlerReserver={this.reserverClick.bind(this)}
                        handlerAnnuler={this.annulerClick.bind(this)}
                        isSelected={isSelected}
                        isReserved={isReserved}
                        noResult={noResult}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = state => {
    return {
        clubId: state.clubId
    }
}
export default connect(mapStateToProps)(Reserver)