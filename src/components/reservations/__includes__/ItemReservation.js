import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import moment from 'moment'
import momentDurationFormatSetup from 'moment-duration-format'
momentDurationFormatSetup(moment)

function _checkNbPlaces(places) {
    if(places === 1) {
      return `${places} place restante`
    }
    else {
        return `${places} places restantes`
    }
}

function _hoursFormat(duration) {
    if(duration % 60 === 0) {
        return moment(duration).format('H[h]')
    }
    else if(duration < 60) {
        return `${duration}min`
    }
    else {
        return moment.duration(duration, "minutes").format('H[h]mm')
    }
}

function _ckeckIsFull(props) {
    const { hours, duration, places, selected, hasReservated} = props
    if(places > 0 && !hasReservated) {
        return (
            <View style={[styles.item, selected && styles.itemSelected]}>
                <Text style={[styles.hours, selected && styles.hoursSelect]}>{hours}</Text>
                <Text style={[styles.duration, selected && styles.durationSelect]}>{_hoursFormat(duration)}</Text>
                <View style={[styles.placesBlock, selected && styles.placesBlockSelected]}>
                    <Text style={[styles.places, selected && styles.placesSelected]}>{_checkNbPlaces(places)}</Text>
                </View>
            </View>
        )
    }
    else if(hasReservated) {
        return (
            <View style={[styles.item, selected && styles.itemAnnuler]}>
                <Text style={[styles.hours, selected && styles.hoursSelect]}>{hours}</Text>
                <Text style={[styles.duration, selected && styles.durationSelect]}>{_hoursFormat(duration)}</Text>
                <View style={[styles.placesBlockReserve, selected && styles.placesBlockAnnuler]}>
                    <Text style={[styles.placesReserve, selected && styles.placesSelected]}>Inscrit</Text>
                </View>
            </View>
        )
    }
    else if(places <= 0) {
        return (
            <View style={styles.itemFull}>
                <Text style={styles.hoursFull}>{hours}</Text>
                <Text style={styles.durationFull}>{_hoursFormat(duration)}</Text>
                <View style={styles.placesBlockFull}>
                    <Text style={styles.placesFull}>Complet</Text>
                </View>
            </View>
        )
    }
}

function formatDate(props) {
    const { currentDay } = props

    let titleDays = moment(currentDay).format("ddd D MMMM")
    let daySplit = titleDays.split(" ")
    let dayName = daySplit[0].charAt(0).toUpperCase() + daySplit[0].slice(1)
    
    return `${dayName} ${daySplit[1]} ${daySplit[2]}`
}

function isAlreadyReserved(props) {
    const { hours, duration, selected } = props
    return (
        <View style={[styles.item, selected && styles.itemAnnuler]}>
            <Text style={[styles.hours, selected && styles.hoursSelect]}>{formatDate()}</Text>
            <Text style={[styles.hours, selected && styles.hoursSelect]}>{hours}</Text>
            <Text style={[styles.duration, selected && styles.durationSelect]}>{_hoursFormat(duration)}</Text>
        </View> 
    )
}

export default function ItemReservation(props) {

    if(props.alreadyReserved) {
        return isAlreadyReserved()
    }
    return _ckeckIsFull()
}

const styles = StyleSheet.create({
    item: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    itemFull:{
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#fff"
    },
    hours:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#33335d'
    },
    duration:{
        fontSize: 19,
        color: '#94a2b3'
    },
    placesBlock:{
       backgroundColor: '#dff8ed',
       width: 170,
       paddingTop: 6,
       paddingBottom: 6,
       paddingLeft: 10,
       paddingRight: 10,
       alignItems: 'center',
       borderRadius: 15
    },
    places:{
        color: '#51c794',
        fontSize: 15
    },
    hoursFull:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#33335d'
    },
    durationFull:{
        fontSize: 19,
        color: '#9e9e9e'
    },
    nameFull:{
        fontSize: 19,
        color: '#9e9e9e'
    },
    placesBlockFull:{
       width: 170,
       paddingTop: 5,
       paddingBottom: 5,
       alignItems: 'center',
       borderRadius: 15
    },
    placesFull:{
        color: '#9e9e9e',
        fontSize: 16
    },
    placesBlockReserve:{
        backgroundColor: '#D62D2D10',
        width: 170,
        paddingTop: 5,
        paddingBottom: 5,
        alignItems: 'center',
        borderRadius: 15
    },
    placesReserve:{
        color: '#B44B4B',
        fontSize: 16
    },
    itemSelected:{
        backgroundColor: '#5e72e4'
    },
    itemAnnuler:{
        backgroundColor: '#e04c4c'
    },
    hoursSelect:{
        color: '#fff'
    },
    durationSelect:{
        color: '#fff'
    },
    placesBlockSelected:{
        backgroundColor: '#ffffff25'
    },
    placesBlockAnnuler:{
        backgroundColor: '#ffffff25'
    },
    placesSelected:{
        color: '#fff'
    }
})