import React from 'react'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'
import ItemReservation from '../../utils/ItemReservation'
import moment from 'moment'
import ItemNoResult from '../../utils/ItemNoResult'

import styles from '../../../styles/ListReservations'

function handlerSelected(props, sessionId, places, index, has_reservated) {
    props.handlerSelected(sessionId, places, index, has_reservated)
}

function handlerRefresh(props) {
    props.handlerRefresh()
}

function handlerReserver(props) {
    props.handlerReserver()
}

function handlerAnnuler(props, reservationId) {
    props.handlerAnnuler(reservationId)
}

function _showReservationBtn(props) {
    if(props.isSelected) {
        return (
            <TouchableOpacity style={styles.reservationBtn} onPress={() => handlerReserver(props)}>
                <Text style={styles.reservationTxt}>Réserver</Text>
            </TouchableOpacity>
        )
    }
    if(props.isReserved) {
        return (
            <TouchableOpacity style={styles.annulationBtn} onPress={() => handlerAnnuler(props)}>
                <Text style={styles.reservationTxt}>Annuler la réservation</Text>
            </TouchableOpacity>
        )
    }
}

export default function ListReservation(props) {

    const { selected, reservations, isFetching, noResult } = props
    let nbNotDisplay = 0

    if(reservations.length >= 1 && !noResult) {
        return (
            <View style={styles.containerReserver}>
                <View style={styles.containerList}>
                    <FlatList 
                        contentContainerStyle={{ paddingBottom: 10 }}
                        data={reservations}
                        onRefresh={() => handlerRefresh(props)}
                        refreshing={isFetching}
                        renderItem={({item, index}) => {
                            let future = moment.unix(item.event_at).add(1, 'hours')
                            let today = moment().add(1, 'hours')
                            if(today.diff(future) <= 0) {
                                return (
                                    <TouchableOpacity activeOpacity={1} onPress={() => handlerSelected(props, item.id, item.remaining_places, index, item.has_reservated)}>
                                        <ItemReservation 
                                            id={item.id} 
                                            hours={moment.unix(item.event_at).format('HH[h]mm')} 
                                            duration={item.duration} 
                                            places={item.remaining_places} 
                                            hasReservated={item.has_reservated} 
                                            selected={selected[index]}
                                        />
                                    </TouchableOpacity>
                                )
                            }
                            nbNotDisplay++
                            if(reservations.length == nbNotDisplay) {
                                nbNotDisplay = 0
                                return (
                                    <TouchableOpacity activeOpacity={1}>
                                        <ItemNoResult msg="Aucune sessions pour ce jour" img="calendar"/>
                                    </TouchableOpacity>
                                )
                            }
                        }}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
                {_showReservationBtn(props)}
            </View>
        )
    }
    else if(noResult) {
        return (
            <FlatList 
                data={reservations}
                onRefresh={() => handlerRefresh(props)}
                refreshing={isFetching}
                renderItem={({item, index}) => (
                    <TouchableOpacity activeOpacity={1}>
                        <ItemNoResult msg="Aucune sessions pour ce jour" img="calendar"/>
                    </TouchableOpacity>
                )}
                keyExtractor={item => item.id.toString()}
            />
        )
    }
    else {
        return null
    }

}