import React, { Component } from 'react'
import {  Text, View, TouchableOpacity } from 'react-native'
import SvgUri from 'expo-svg-uri'
import moment from 'moment'
import 'moment/locale/fr'
moment.locale('fr')

import styles from '../../../styles/Calendar'

export default class CalendarBar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            beforeDay: -1,
            nextDay: 1,
            today: moment().format("YYYY-MM-DD"),
            currentDay: moment().format("YYYY-MM-DD")
        }
    }

    // Format title day
    _formatDateBar() {
        const { currentDay, today } = this.state

        if(currentDay == today) {
            return "Aujourd'hui"
        }
        else if(currentDay == moment().add(1, 'days').format("YYYY-MM-DD")) {
            return "Demain"
        }
        else {
            let titleDays = moment(currentDay).format("dddd D MMMM")
            let daySplit = titleDays.split(" ")
            let dayName = daySplit[0].charAt(0).toUpperCase() + daySplit[0].slice(1)
            return `${dayName} ${daySplit[1]} ${daySplit[2]}`
        }
    }

    // Handler before day
    selectBeforeDay() {
        const { beforeDay, nextDay } = this.state
        if(beforeDay >= 0) {
            let day = moment().add(beforeDay, 'days').format("YYYY-MM-DD")
            this.props.day(day)
            this.setState({
                beforeDay: beforeDay - 1,
                nextDay: nextDay - 1,
                currentDay: day
            })
        }
    }

    // Handler next day
    selectNextDay() {
        const { beforeDay, nextDay } = this.state
        if(nextDay >= 1 && nextDay < 15) {
            let day = moment().add(nextDay, 'days').format("YYYY-MM-DD")
            this.props.day(day)
            this.setState({
                beforeDay: beforeDay + 1,
                nextDay: nextDay + 1,
                currentDay: day
            })
        }
    }

    render() {
        return (
            <View style={styles.dateBarContainer}>
                <TouchableOpacity activeOpacity={1} onPress={() => this.selectBeforeDay()}>
                    <SvgUri style={this.state.beforeDay == -1 ? styles.btnOpacity : styles.none} width={20} source={require('../../../assets/icon/date/arrow-date-left.svg')} />
                </TouchableOpacity>

                <Text style={styles.titleDate}>{this._formatDateBar()}</Text>
                
                <TouchableOpacity activeOpacity={1} onPress={() => this.selectNextDay()}>
                    <SvgUri style={this.state.nextDay == 15 ? styles.btnOpacity : styles.none} width={20} source={require('../../../assets/icon/date/arrow-date-right.svg')} />
                </TouchableOpacity>
            </View>
        )
    }
    
}