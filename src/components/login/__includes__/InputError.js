import React from 'react';
import { StyleSheet, Text } from 'react-native';

export default function InputError(props) {
  
    const { msg, errorInput, warningMsg } = props
    if(errorInput) {
        return (
            <Text style={[styles.errorTxt, warningMsg && styles.warningMsg]}>{msg}</Text>
        )
    }
    else {
        return null;
    }
}

const styles = StyleSheet.create({
    errorTxt: {
        marginTop: 20,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 3,
        paddingRight: 3,
        borderRadius: 5,
        backgroundColor: '#D62D2D10',
        fontSize: 17,
        textAlign: 'center',
        color: '#B44B4B'
      },
      warningMsg:{
        backgroundColor: '#daa24f24',
        color: '#daa24f'
      }
})