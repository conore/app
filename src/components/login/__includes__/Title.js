import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import SvgUri from 'expo-svg-uri'

export default function Title(props) {
    return (
        <View style={styles.loginHeader}>
            <View style={styles.loginLogo}>
                <SvgUri width={50} height={50} source={require('../../../assets/logo/ic-conore.svg')} />
                <Text style={styles.loginTitle}>conore</Text>
            </View>
            
            <Text style={styles.loginSubtitle}>Réservez facilement des séances dans vos clubs</Text>
        </View>

    )
}

const styles = StyleSheet.create({
    loginHeader: {
        marginTop: 100
     },
     loginLogo: {
         flexDirection: 'row',
         alignItems: 'center'
     },
     loginTitle: {
         fontSize: 40,
         marginLeft: 10,
         color: "#191ead",
         
     },
     loginSubtitle: {
         fontSize: 27,
         marginTop: 30
    },
})