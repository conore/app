import React from 'react'
import { StyleSheet, View, TextInput } from 'react-native'
import InputError from './InputError'

function handlerNewPassword(props, newPassword) {
    props.handlerNewPassword(newPassword)
}

function handlerConfirmPassword(props, confirmPassword) {
    props.handlerConfirmPassword(confirmPassword)
}

export default function InputLogin(props) {
  
    const { newPassword, confirmPassword, warningMsg, errorInput, errorMessage } = props
    return (
        <View>
            <TextInput
                style={styles.inputPassword}
                onChangeText={text => handlerNewPassword(props, text)}
                placeholder='Nouveau mot de passe'
                secureTextEntry={true}
                autoCapitalize='none'
                value={newPassword}
                selectionColor='#5e72e4'
                underlineColorAndroid='transparent'
            />

            <TextInput
                style={styles.inputPassword}
                onChangeText={text => handlerConfirmPassword(props, text)}
                placeholder='Confirmer mot de passe'
                secureTextEntry={true}
                autoCapitalize='none'
                value={confirmPassword}
                selectionColor='#5e72e4'
                underlineColorAndroid='transparent'
            />

            <InputError
                msg={errorMessage}
                errorInput={errorInput}
                warningMsg={warningMsg}
            />
        </View>
    )
        
}


const styles = StyleSheet.create({
    inputEmail:{
        marginTop: 20,
        height: 50,
        marginTop: 20,
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 22,
        borderColor: '#c1cad3',
        borderWidth: 1
    },
    inputPassword:{
        height: 50,
        marginTop: 20,
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 22,
        borderColor: '#c1cad3',
        borderWidth: 1
    },
})