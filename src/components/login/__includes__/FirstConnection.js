import React, { Component } from 'react'
import { View, SafeAreaView, Text, TouchableOpacity, TouchableWithoutFeedback, KeyboardAvoidingView, Platform, Keyboard } from 'react-native'
import SvgUri from 'expo-svg-uri'
import ApiRequest from '../../../api/ApiRequest'
import { userLogin } from '../../../api/__includes__/authentication'
import InputFirstConnection from './InputFirstConnection'
import m from '../../utils/messages'

import styles from '../../../styles/FirstConnection'

export default class FirstConnection extends Component {

    constructor(props) {
        super(props)
        this.state = {
            newPassword: "",
            confirmPassword: "",
            errorInput: false,
            errorMessage: "",
            warningMsg: false,
        }
    }

    handlerNewPassword(newPassword) {
        if(this.state.errorInput) {
            this.setState({ newPassword: newPassword, errorInput: false })
        }
        else {
            this.setState({ newPassword: newPassword })
        }
    }

    handlerConfirmPassword(confirmPassword) {
        if(this.state.errorInput) {
            this.setState({ confirmPassword: confirmPassword, errorInput: false })
        }
        else {
            this.setState({ confirmPassword: confirmPassword })
        }
    }

    handlerSubmit() {
        const { navigate } = this.props.navigation
        let { email } = this.props.navigation.state.params
        let { newPassword, confirmPassword } = this.state

        if(newPassword === confirmPassword) {
            if(newPassword.length > 5) {
                ApiRequest.changePasswordAccount(newPassword).then(async res => {
                    if(res.data.status == 'success') {
                        await userLogin(email, newPassword)

                        ApiRequest.getAccount().then( async res => {
                            if(res.data.status == 'success') {
                                let clubs = res.data.data.clubs
                                let clubIds = []
                                let noClubs = []

                                for(let i = 0; i < clubs.length; i++) {
                                    let subscription = clubs[i].subscriptions
                                    
                                    if(subscription.length > 0) {
                                        clubIds.push(clubs[i].clubId)
                                    }
                                    else {
                                        noClubs.push(clubs[i].clubId)
                                    }
                                }

                                if(noClubs.length > 0) {
                                    if(clubIds.length > 0) {
                                        this.props.navigation.navigate('Home')
                                        SplashScreen.hide()
                                    }
                                    else {
                                        this.props.navigation.navigate('SubscriptionSelection', { clubIds: noClubs })
                                        SplashScreen.hide()
                                    }
                                }
                                else {
                                    this.props.navigation.navigate('Home')
                                    SplashScreen.hide()
                                }
                            }
                        })
                    }
                    else {
                        this.showError(true, true, m('general/error'))
                        return;
                    }
                })
                .catch(error => {
                    this.showError(true, true, m('general/error'))
                    return;
                })
            }
            else {
                this.showError(true, true, m('auth/invalid-new-password'))
                return;
            }
        }
        else {
            this.showError(true, false, m('auth/invalid-confirm-password'))
            return;
        }
    }

    backLogin() {
        this.props.navigation.navigate("Login")
    }

    showError(errorInput, warningMsg, errorMessage) {
        this.setState({
            errorInput: errorInput,
            warningMsg: warningMsg,
            errorMessage: errorMessage
        })
    }

    render() {
        let { newPassword, confirmPassword, errorInput, errorMessage, warningMsg } = this.state

        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <KeyboardAvoidingView
                    style={{flex: 1}}
                    behavior={Platform.OS === "ios" ? "padding" : null}
                    >
                    <SafeAreaView style={styles.container}>
                        <View>
                            <SvgUri width={150} style={{alignItems: 'center', marginTop: 100}} source={require('../../../assets/icon/profile/yoga.svg')} />
                            <Text style={styles.title}>Bienvenue sur Conore !</Text>
                            <Text style={styles.subtitle}>Commencez par changer votre mot de passe</Text>
                        </View>

                        <InputFirstConnection 
                            newPassword={newPassword} 
                            confirmPassword={confirmPassword} 
                            errorMessage={errorMessage} 
                            errorInput={errorInput} 
                            warningMsg={warningMsg}
                            handlerNewPassword={this.handlerNewPassword.bind(this)}
                            handlerConfirmPassword={this.handlerConfirmPassword.bind(this)}
                        />

                        <View>
                            <TouchableOpacity activeOpacity={1} onPress={() => this.handlerSubmit()} style={styles.submitBtn}>
                                <Text style={styles.submitBtnTxt}>Valider</Text>
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={1} onPress={() => this.backLogin()}>
                                <Text style={styles.backLoginTxt}>Retour</Text>
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>

                </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
        )
    }
}