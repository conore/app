import React from 'react'
import { StyleSheet, View, TextInput, Text } from 'react-native'
import InputError from './InputError'

function handlerEmail(props, email) {
    props.handlerEmail(email)
}

function handlerPassword(props, password) {
    props.handlerPassword(password)
}

export default function InputLogin(props) {
  
    const { email, password, warningMsg, errorInput, errorMessage } = props
    return (
        <View>
            <Text style={styles.connexionTxt}>Connexion</Text>

            <TextInput
                style={styles.inputEmail}
                onChangeText={text => handlerEmail(props, text)}
                placeholder='Email'
                keyboardType='email-address'
                autoCapitalize='none'
                value={email}
                selectionColor='#5e72e4'
                underlineColorAndroid="transparent"
            />

            <TextInput
                style={styles.inputPassword}
                onChangeText={text => handlerPassword(props, text)}
                placeholder='Mot de passe'
                secureTextEntry={true}
                autoCapitalize='none'
                value={password}
                selectionColor='#5e72e4'
                underlineColorAndroid='transparent'
            />

            <InputError
                msg={errorMessage}
                errorInput={errorInput}
                warningMsg={warningMsg}
            />
        </View>
    )
        
}


const styles = StyleSheet.create({
    connexionTxt: {
        marginTop: 10,
        fontSize: 25,
        color: '#525252',
        fontWeight: 'bold'
    },
    inputEmail:{
        marginTop: 20,
        height: 50,
        marginTop: 20,
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 22,
        borderColor: '#c1cad3',
        borderWidth: 1
    },
    inputPassword:{
        height: 50,
        marginTop: 20,
        borderRadius: 5,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 22,
        borderColor: '#c1cad3',
        borderWidth: 1
    },
})