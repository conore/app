import React, { Component } from 'react'
import { SafeAreaView, StatusBar, TouchableOpacity, Text, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Platform, AsyncStorage, View } from 'react-native'
import ApiRequest from '../../api/ApiRequest'
import { userAuth, userLogin } from '../../api/__includes__/authentication'
import * as EmailValidator from 'email-validator'
import m from '../utils/messages'

import Title from './__includes__/Title'
import InputLogin from './__includes__/InputLogin'

import styles from '../../styles/Login'
import SplashScreen from 'react-native-splash-screen'

export default class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: "",
            errorInput: false,
            errorMessage: "",
            warningMsg: false,
            isLoading: true
        }
    }

    async componentDidMount() {
        let accessToken = await AsyncStorage.getItem("accessToken")
        let refreshToken = await AsyncStorage.getItem("refreshToken")
        let email = await AsyncStorage.getItem("email")
        let password = await AsyncStorage.getItem("password")

        if(accessToken !== "") {
            ApiRequest.login(email, password).then(async res => {

                if(res.data.status == 'success') {
                    accessToken = res.data.data.accessToken
                    refreshToken = res.data.data.refreshToken
                    let firstConnection = res.data.data.first_connection   
                    
                    await userAuth(accessToken, refreshToken)
                    
                    if(!firstConnection) {
                        ApiRequest.getAccount().then( async res => {
                            if(res.data.status == 'success') {
                                let clubs = res.data.data.clubs
                                let clubIds = []
                                let noClubs = []

                                for(let i = 0; i < clubs.length; i++) {
                                    let subscription = clubs[i].subscriptions
                                    
                                    if(subscription.length > 0) {
                                        clubIds.push(clubs[i].clubId)
                                    }
                                    else {
                                        noClubs.push(clubs[i].clubId)
                                    }
                                }
                                
                                if(noClubs.length > 0) {
                                    if(clubIds.length > 0) {
                                        this.props.navigation.navigate('Home')
                                        SplashScreen.hide()
                                    }
                                    else {
                                        this.props.navigation.navigate('SubscriptionSelection', { clubIds: noClubs })
                                        SplashScreen.hide()
                                    }
                                }
                                else {
                                    this.props.navigation.navigate('Home')
                                    SplashScreen.hide()
                                }
                            }
                        })
                        SplashScreen.hide()
                    }
                    else {
                        this.setState({ isLoading: false })
                    }
                }
            })
            .catch(error => {
                this.setState({ isLoading: false })
            })
        }
        else {
            this.setState({ isLoading: false })
        }
    }

    handlerEmail(email) {
        if(this.state.errorInput) {
            this.setState({ email: email, errorInput: false })
        }
        else {
            this.setState({ email: email })
        }
    }

    handlerPassword(password) {
        if(this.state.errorInput) {
            this.setState({ password: password, errorInput: false })
        }
        else {
            this.setState({ password: password })
        }
    }

    handlerConnectionClicked() {
        const { navigate } = this.props.navigation
        const { email, password } = this.state

        if(email.trim().length > 0 && password.trim().length > 0) {
            let validator = EmailValidator.validate(email)

            if(!validator) {
                this.showError(true, false, m('auth/invalid-email'))
                return;
            }

            ApiRequest.login(email, password).then(async res => {
                if(res.data.status == 'success') {
                    let accessToken = res.data.data.accessToken
                    let refreshToken = res.data.data.refreshToken
                    let firstConnection = res.data.data.first_connection   
                    
                    await userAuth(accessToken, refreshToken)
                    await userLogin(email, password)

                    if(!firstConnection) {
                        ApiRequest.getAccount().then( async res => {
                            if(res.data.status == 'success') {
                                let clubs = res.data.data.clubs
                                let clubIds = []
                                let noClubs = []

                                for(let i = 0; i < clubs.length; i++) {
                                    let subscription = clubs[i].subscriptions
                                    
                                    if(subscription.length > 0) {
                                        clubIds.push(clubs[i].clubId)
                                    }
                                    else {
                                        noClubs.push(clubs[i].clubId)
                                    }
                                }

                                if(noClubs.length > 0) {
                                    if(clubIds.length > 0) {
                                        this.props.navigation.navigate('Home')
                                        SplashScreen.hide()
                                    }
                                    else {
                                        this.props.navigation.navigate('SubscriptionSelection', { clubIds: noClubs })
                                        SplashScreen.hide()
                                    }
                                }
                                else {
                                    this.props.navigation.navigate('Home')
                                    SplashScreen.hide()
                                }
                            }
                        })
                    }
                    else {
                        navigate('FirstConnection', { email, password })
                    }
                }
                else {
                    this.showError(true, false, m('auth/invalid-form'))
                    return;
                }
            })
            .catch(error => {
                if(error.includes("invalid_credentials")) {
                    this.showError(true, false, m('auth/invalid-form'))
                }
                else if(error.includes("Authentication failed")) {
                    this.showError(true, false, m('auth/invalid-form'))
                }
                else {
                    this.showError(true, true, m('general/error-network'))
                }
                return;
            })
        }
        else {
            this.showError(true, false, m('auth/invalid-form-empty'))
            return;
        }
    }

    showError(errorInput, warningMsg, errorMessage) {
        this.setState({
            errorInput: errorInput,
            warningMsg: warningMsg,
            errorMessage: errorMessage
        })
    }

    render() {
        let { email, password, errorMessage, errorInput, warningMsg, isLoading } = this.state

        if(!isLoading) {
            SplashScreen.hide()
            return (
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <KeyboardAvoidingView
                        style={{flex: 1}}
                        behavior={Platform.OS === "ios" ? "padding" : null}
                        >
                        <SafeAreaView style={styles.container}>
                            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
    
                            <Title />
    
                            <InputLogin 
                                email={email} 
                                password={password} 
                                errorMessage={errorMessage} 
                                errorInput={errorInput} 
                                warningMsg={warningMsg}
                                handlerEmail={this.handlerEmail.bind(this)}
                                handlerPassword={this.handlerPassword.bind(this)}
                            />
    
                            <TouchableOpacity activeOpacity={1} onPress={() => this.handlerConnectionClicked()} style={styles.loginBtn}>
                                <Text style={styles.loginBtnTxt}>Se connecter</Text>
                            </TouchableOpacity>
                        </SafeAreaView>
                        
                    </KeyboardAvoidingView>
                </TouchableWithoutFeedback>
            )
        }
        else {
            if(this.props.navigation.state.params != undefined) {
                SplashScreen.hide()
                return (
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <KeyboardAvoidingView
                            style={{flex: 1}}
                            behavior={Platform.OS === "ios" ? "padding" : null}
                            >
                            <SafeAreaView style={styles.container}>
                                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
        
                                <Title />
        
                                <InputLogin 
                                    email={email} 
                                    password={password} 
                                    errorMessage={errorMessage} 
                                    errorInput={errorInput} 
                                    warningMsg={warningMsg}
                                    handlerEmail={this.handlerEmail.bind(this)}
                                    handlerPassword={this.handlerPassword.bind(this)}
                                />
        
                                <TouchableOpacity activeOpacity={1} onPress={() => this.handlerConnectionClicked()} style={styles.loginBtn}>
                                    <Text style={styles.loginBtnTxt}>Se connecter</Text>
                                </TouchableOpacity>
                            </SafeAreaView>
                            
                        </KeyboardAvoidingView>
                    </TouchableWithoutFeedback>
                )
            }
            else {
                return <View></View>
            }
        }
    }
}
