import React, { Component } from 'react'
import { View, SafeAreaView, StatusBar, Text, ScrollView, StyleSheet, TouchableOpacity,BackHandler } from 'react-native'
import HeaderBar from '../profile/__includes__/HeaderBar'
import ItemSubscription from './__includes__/ItemSubscription'
import { TextInput } from 'react-native-gesture-handler'
import SvgUri from 'expo-svg-uri'
import moment from 'moment'
import ApiRequest from '../../api/ApiRequest'
import Stripe from 'react-native-stripe-api'
const apiKey = 'sk_test_7Nxtobfa1OwFqAYqQy1s1KdD00WhYeaYNV' // test key
const stripe = new Stripe(apiKey);

export default class Payments extends Component {

    constructor(props) {
        super(props)
        this.state = {
            number: "",
            month: "",
            expMonth: "",
            expYear: "",
            cvc: ""
        }
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.activeBtn()
        })
    }

    async payClick(subscriptions) {
        let { number, expMonth, expYear, cvc } = this.state
        let { navigate } = this.props.navigation.state.params

        const token = await stripe.createToken(number, expMonth, expYear, cvc)

        if(this.addSubscription(subscriptions, token.id)) {
            this.props.navigation.navigate(navigate)
        }
        else {
            console.log("Il y a eu une erreur")
        }
    }

    async addSubscription(subscriptions, token) {
        let result = false
        await subscriptions.map(async subscription => {
            await ApiRequest.setClubSubscription(subscription.clubId, token, subscription.name).then(res => {
                result = true
            })
            .catch(err => {
                console.log(err)
            })
        })
        return result
    }

    handlerNumber(nb) {
        let { number } = this.state
    
        if(nb.length < 20) {
            if((nb.length == 4 && number.length == 3) || (nb.length == 9 && number.length == 8) || (nb.length == 14 && number.length == 13)) {
                nb += " "
            }
            if((nb.length == 15 && number.length == 14) || (nb.length == 10 && number.length == 9) || (nb.length == 5 && number.length == 4)) {
                nb = nb.split(/(?=.{1}$)/).join(' ');
            }
            if((nb.length == 5 && number.length == 6) || (nb.length == 10 && number.length == 11) || (nb.length == 15 && number.length == 16)) {
                nb = nb.slice(0, -1)
            }
            this.setState({ number: nb })
        }
    }
    handlerMonth(mm) {
        let { month } = this.state
    
        if(mm.length < 6) {
            let expMonth = ""
            let expYear = ""
            if(mm.length == 2 && month.length == 1) {
                expMonth = mm
                mm += "/"
            }
            if(mm.length == 3 && month.length == 2) {
                mm = mm.split(/(?=.{1}$)/).join('/')
            }
            if(mm.length == 5 && month.length == 4) {
                let split = mm.split("/")
                expMonth = split[0]
                expYear = split[1]
            }
            if(mm.length == 3 && month.length == 4) {
                mm = mm.slice(0, -1)
            }
            this.setState({ month: mm, expMonth: expMonth, expYear: expYear })
        }
    }

    handlerCvc(cvc){
        this.setState({ cvc: cvc })
    }

    backBtn() {
        this.props.navigation.goBack()
    }

    displayPayBtn(subscriptions) {
        let { number, expMonth, expYear, cvc } = this.state

        number = number.replace(/\s/g, "")
        expMonth = expMonth.replace(/\s/g, "")
        expYear = expYear.replace(/\s/g, "")
        cvc = cvc.replace(/\s/g, "")

        if(expMonth <= 12 && expMonth > 0 && expYear >= moment().format("YY")) {
            let expireDate = moment.utc()
            expireDate.set('year', `20${expYear}`)
            expireDate.set('month', expMonth)
            expireDate.set('date', 1)
            expireDate.startOf('day')
            expireDate.format('x')
    
            let start_date = moment()
            let days = expireDate.diff(start_date,'days')

            if(days >= 0) {
                if((!isNaN(number) && number.length == 16) && (!isNaN(expMonth) && expMonth.length == 2) && (!isNaN(expYear) && expYear.length == 2) && (!isNaN(cvc) && cvc.length == 3)) {
                    return (
                        <TouchableOpacity activeOpacity={1} onPress={() => this.payClick(subscriptions)} style={styles.confirmBtnActive}>
                            <Text style={styles.confirmBtnTxtActive}>Payer</Text>
                        </TouchableOpacity>
                    )
                }
                else {
                    return (
                        <View style={styles.confirmBtn}>
                            <Text style={styles.confirmBtnTxt}>Payer</Text>
                        </View>
                    )
                }
            }
            else {
                return (
                    <View style={styles.confirmBtn}>
                        <Text style={styles.confirmBtnTxt}>Payer</Text>
                    </View>
                )
            }
        }
        else {
            return (
                <View style={styles.confirmBtn}>
                    <Text style={styles.confirmBtnTxt}>Payer</Text>
                </View>
            )
        }
    }

    renderItem(subscriptions) {
        return subscriptions.map(item => {
            return (
                <View>
                    <ItemSubscription subscription={item} />
                </View>
            )
        })
    }

    render() {
        let { subscriptions, totalPrice } = this.props.navigation.state.params
        let { number, month, cvc } = this.state

        return(
            <SafeAreaView style={{ flex: 1 }}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <HeaderBar title="Paiement" backBtn={this.backBtn.bind(this)}/>
                <ScrollView>
                    <View>
                        <Text style={styles.resume}>Votre commande</Text>
                        {this.renderItem(subscriptions)}
                        <View style={styles.totalBlock}>
                            <Text style={styles.totalTitle}>Total</Text>
                            <Text style={styles.total}>{totalPrice}€</Text>
                        </View>

                        <Text style={styles.paiementTitle}>Paiement</Text>
                        <View style={styles.numberInputBlock}>
                            <SvgUri width={50} style={{alignItems: 'center'}} source={require('../../assets/icon/payment/pay.svg')} />
                            <TextInput
                                style={styles.numberInput}
                                onChangeText={text => this.handlerNumber(text)}
                                placeholder='Numéro de vore carte'
                                keyboardType='number-pad'
                                maxLength={20}
                                value={number}
                                underlineColorAndroid="transparent"
                            />
                        </View>

                        <View style={styles.expireBlock}>
                            <View style={styles.expireInputBlock}>
                                <SvgUri width={50} style={{alignItems: 'center'}} source={require('../../assets/icon/payment/month.svg')} />
                                <TextInput
                                    style={styles.monthInput}
                                    onChangeText={text => this.handlerMonth(text)}
                                    placeholder='mm/aa'
                                    keyboardType='number-pad'
                                    maxLength={5}
                                    value={month}
                                    underlineColorAndroid="transparent"
                                />
                            </View>
                            <View style={styles.cvcInputBlock}>
                                <SvgUri width={25} style={{alignItems: 'center'}} source={require('../../assets/icon/payment/lock.svg')} />
                                <TextInput
                                    style={styles.cvcInput}
                                    onChangeText={text => this.handlerCvc(text)}
                                    placeholder='CVC'
                                    keyboardType='number-pad'
                                    maxLength={3}
                                    value={cvc}
                                    underlineColorAndroid="transparent"
                                />
                            </View>
                        </View>
                        {this.displayPayBtn(subscriptions)}
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    resume: {
        fontSize: 22,
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 15,
        borderWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#e8e8e8",
    },
    totalBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomWidth: 1,
        borderColor: "#e8e8e8",
    },
    totalTitle: {
        fontSize: 19,
    },
    total: {
        fontSize: 19,
    },
    paiementTitle: {
        fontSize: 22,
        marginTop: 30,
        marginBottom: 20,
        paddingLeft: 15,
    },
    numberInputBlock: {
        flexDirection: 'row',
        marginLeft: 20,
        marginRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 5,
        borderWidth: 1,
        borderColor: "#5e72e4"
    },
    numberInput: {
        fontSize: 20,
    },
    monthInput: {
        fontSize: 20,
    },
    expireBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15
    },
    expireInputBlock: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        marginRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 5,
        borderWidth: 1,
        borderColor: "#5e72e4"
    },
    cvcInputBlock: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 5,
        borderWidth: 1,
        borderColor: "#5e72e4",
        alignItems: 'center'
    },
    cvcInput: {
        fontSize: 20,
        marginLeft: 10
    },
    confirmBtnActive: {
        borderRadius: 5,
        backgroundColor: '#4960de',
        marginTop: 70,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 30,
        paddingRight: 25,
        paddingLeft: 25,
        paddingTop: 10,
        paddingBottom: 10,
    }, 
    confirmBtnTxtActive: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },
    confirmBtn: {
        borderRadius: 5,
        backgroundColor: '#f0f0f0',
        marginTop: 70,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 30,
        paddingRight: 25,
        paddingLeft: 25,
        paddingTop: 10,
        paddingBottom: 10,
    }, 
    confirmBtnTxt: {
        textAlign: 'center',
        color: '#c7c7c7',
        fontSize: 20,
        fontWeight: 'bold'
    }
})