import React from 'react';
import { Text, View, StyleSheet } from 'react-native'


function frequencyFormat(frequency) {
    if(frequency == "day") {
        return "jour"
    }
    if(frequency == "week") {
        return "semaine"
    }
    if(frequency == "month") {
        return "mois"
    }
    if(frequency == "year") {
        return "an"
    }
}

export default function ItemSubscription(props) {

    const { subscription } = props

    return (
        <View style={styles.abonnementPayment}>
            <View>
                <Text style={styles.title}>{subscription.name}</Text>
                <Text style={styles.subTitle}>Abonnement</Text>
                <Text style={styles.subTitle}>{`Club - ${subscription.clubName}`}</Text>
            </View>

            <View style={styles.subIcon}>
                <Text style={styles.price}>{`${subscription.amount}€/${frequencyFormat(subscription.interval)}`}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    abonnementPayment: {
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: "#e8e8e8",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'center'
    }, 
    title: {
        fontSize: 19,
    },
    subTitle: {
        fontSize: 17,
        color: "#6b6b6b",
        marginLeft: 10,
        marginTop: 5
    },
    price: {
        color: '#ffb016',
        fontSize: 19
    }, 
})