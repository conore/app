import React from "react"
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'

import Login from "../components/login/index.js"
import FirstConnection from "../components/login/__includes__/FirstConnection"
import Settings from "../components/settings/index.js"
import ListClubs from "../components/profile/__includes__/ListClubs"
import ListReservations from "../components/profile/__includes__/ListReservations"
import Subscriptions from "../components/subscriptions/index.js"
import Payments from "../components/payments/Payments.js"
import SubscriptionSelection from "../components/subscriptions/__includes__/ SubscriptionSelection.js"

import bottomTabNavigator from './__includes__/BottomNavigation'

const ConversationStackNavigator = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: { header: null }
    },
    Home: {
        screen: bottomTabNavigator,
        navigationOptions: { header: null, gesturesEnabled: false }
    },
    FirstConnection: {
        screen: FirstConnection,
        navigationOptions: { header: null, gesturesEnabled: false }
    },
    Settings: {
        screen: Settings,
        navigationOptions: { header: null }
    },
    ListClubs: {
        screen: ListClubs,
        navigationOptions: { header: null }
    },
    ListReservations: {
        screen: ListReservations,
        navigationOptions: { header: null }
    },
    Subscriptions: {
        screen: Subscriptions,
        navigationOptions: { header: null }
    },
    Payments: {
        screen: Payments,
        navigationOptions: { header: null }
    },
    SubscriptionSelection: {
        screen: SubscriptionSelection,
        navigationOptions: { header: null }
    },
})

export default createAppContainer(ConversationStackNavigator)