import React from "react";
import SvgUri from 'expo-svg-uri'
import { View } from "react-native"

function Profil(color, size) {
    if(color === "#5e72e4") {
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <SvgUri width={size} style={{marginTop: 5}}source={require('../../assets/icon/navbar/icon-profil-active.svg')} />
            </View>
        )
    }
    else {
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <SvgUri width={size} style={{marginTop: 5}} source={require('../../assets/icon/navbar/icon-profil.svg')} />
            </View>
        )
    }
}

function Reserver(color, size) {
    if(color === "#5e72e4") {
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <SvgUri width={size} style={{marginTop: 5}} source={require('../../assets/icon/navbar/icon-reserver-active.svg')} />
            </View>
        )
    }
    else {
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <SvgUri width={size} style={{marginTop: 5}} source={require('../../assets/icon/navbar/icon-reserver.svg')} />
            </View>
        )
    }
}

function Clubs(color, size) {
    if(color === "#5e72e4"){
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <SvgUri  width={size} style={{marginTop: 5}} source={require('../../assets/icon/navbar/icon-clubs-active.svg')} />
            </View>
        )
    }
    else {
        return (
            <View style={{flex: 1, alignItems: 'center'}}>
                <SvgUri width={size} style={{marginTop: 5}} source={require('../../assets/icon/navbar/icon-clubs.svg')} />
            </View>
        )
    }
}


export default  Icon = ({ name, color, size, ...props }) => {
  let icon = null

  switch(name) {
    case "profil":
        icon = Profil(color, size) 
        break
    case "reserver":
        icon = Reserver(color, size) 
        break
    case "clubs":
        icon = Clubs(color, size) 
        break
    default:
        break
  }

  return icon
}