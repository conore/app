import React from "react"
import { createBottomTabNavigator } from "react-navigation-tabs"

import Reserver from "../../components/reservations/index.js"
import Icon from "./Icon"
import Profile from "../../components/profile/index.js"
import Clubs from "../../components/club/index.js"

const bottomTabNavigator = createBottomTabNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarLabel:"Profil",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="profil" size={18} color={tintColor} />
        )
      }
    },
    Reserver: {
      screen: Reserver,
      navigationOptions: {
        tabBarLabel:"Réserver",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="reserver" size={18} color={tintColor} />
        )
      }
    },
    Clubs: {
      screen: Clubs,
      navigationOptions: {
        tabBarLabel:"Club",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="clubs" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    initialRouteName: 'Reserver',
    tabBarOptions: {
      showIcon: true,
      upperCaseLabel: false,
      activeTintColor: '#5e72e4',
      inactiveTintColor: "#ababae",
      activeBackgroundColor: '#fff',
      style: {
        height: 60,
        alignItems: 'center'
      },
      labelStyle: {
        fontSize: 15,
      },
      tabStyle:{
        height: 50,
        backgroundColor: "#fff"
      }
    }
  }
)

export default bottomTabNavigator
