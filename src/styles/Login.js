import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    marginLeft: 30,
    marginRight: 30,
  },
  loginBtn: {
      borderRadius: 5,
      backgroundColor: '#4960de',
      marginTop: 10,
      marginBottom: 30,
      paddingRight: 25,
      paddingLeft: 25,
      paddingTop: 10,
      paddingBottom: 10,
  }, 
  loginBtnTxt: {
      textAlign: 'center',
      color: '#fff',
      fontSize: 20,
      fontWeight: 'bold'
  }
})
