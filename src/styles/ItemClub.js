import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    titleBlock: {
        backgroundColor: "#edf1f7",
        borderRadius: 5
    },
    title: {
        fontSize: 35,
        marginTop: 10,
        textAlign: "center"
    },
    description: {
        fontSize: 20,
        textAlign: "center"
    },
    addressBlock: {
        height: 85,
        marginLeft: 25,
        marginRight: 25,
        marginTop: 25,
        elevation: 1,
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 5
    },
    mapBlock: {
        width: 70,
        marginLeft: 10,
    },
    address: {
        fontSize: 16
    },
    city: {
        fontSize: 15,
        marginTop: 2,
        color: "#707070"
    },
    telBlock:{
        height: 60,
        marginLeft: 25,
        marginRight: 25,
        marginTop: 15,
        marginBottom: 50,
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 5
    },
    telLeft: {
        flexDirection: "row",
    },
    tel: {
        marginLeft: 10,
        fontSize: 16
    }
})
