import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    dateBarContainer: {
        flexDirection: "row",
        backgroundColor: '#5e72e412',
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        height: 60, 
    },
    titleDate: {
        fontSize: 19,
        color: "#5e72e4",
        fontWeight: "bold"
    },
    btnOpacity: {
        opacity: 0.3
    }
})
