import { StyleSheet, Platform } from 'react-native'

export default styles = StyleSheet.create({
    viewProfil:{ 
        flex: 1,
    },
    topBar: {
        height: 60,
        backgroundColor: '#fff',
        borderBottomWidth: 0.5,
        borderBottomColor: '#d6d6d6',
        shadowColor: '#d6d6d65e',
        elevation: 2,
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 2,
        shadowOpacity: 1.0
    },
    alignTopBar: {
        ...Platform.select({
            ios: {
                flex: 1, 
                justifyContent: 'space-between',
            }
            }),
        flexDirection: "row",
        alignItems: "center"
    },
    topBarTxt:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#5e72e4',
        ...Platform.select({
            ios: {
                width: 300,
                textAlign: 'center',
            },
            android: {
                marginLeft: 15
            },
          }),
    },
    backBtn: {
        marginLeft: 15
    },
    titleClubSelected: {
        color: "#5e72e4"
    },
    clubNameBlock: {
        backgroundColor: '#5e72e412',
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    titleClubName: {
        fontSize: 18,
        marginLeft: 20,
        paddingTop: 15,
        paddingBottom: 15,
    },
    titleClub: {
        fontSize: 17,
        color: "#5e72e4",
        fontWeight: "bold"
    },
})
