import { StyleSheet } from 'react-native'

export default  styles = StyleSheet.create({
    viewProfil:{ 
        flex: 1,
    },
    topBar: {
        height: 60,
        backgroundColor: '#fff',
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#d6d6d6',
        shadowColor: '#d6d6d65e',
        elevation: 2,
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 2,
        shadowOpacity: 1.0
    },
    topBarTxt:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#5e72e4',
        marginLeft: 15
    },
    errorNetworkContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorMsg: {
        width: 300,
        textAlign: 'center',
        marginTop: 15,
        fontSize: 18
    },
    deconnexion: {
        marginLeft: 20,
        marginRight: 20,
        fontSize: 19,
        color: "#787878"
    },
})