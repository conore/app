import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    loading_container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
