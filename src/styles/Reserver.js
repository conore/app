import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    viewReserver:{ 
        flex: 1,
    },
    btnOpacity: {
        opacity: 0.3
    }
})