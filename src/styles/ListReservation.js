import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    viewProfil:{ 
        flex: 1,
    },
    topBar: {
        height: 60,
        backgroundColor: '#fff',
        borderBottomWidth: 0.5,
        borderBottomColor: '#d6d6d6',
        shadowColor: '#d6d6d65e',
        elevation: 2,
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 2,
        shadowOpacity: 1.0
    },
    topBarTxt:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#5e72e4',
        marginLeft: 15
    },
    alignTopBar: {
        flexDirection: "row",
        alignItems: "center"
    },
    backBtn: {
        marginLeft: 15
    },
    annulationBtn:{
        backgroundColor: '#e04c4c',
        paddingTop: 15,
        paddingBottom: 15,
        justifyContent: 'flex-end',
        elevation: 1
    },
    reservationTxt:{
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    clubNameBlock: {
        backgroundColor: '#5e72e412',
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    titleClub: {
        fontSize: 17,
        color: "#5e72e4",
        fontWeight: "bold"
    }
})
