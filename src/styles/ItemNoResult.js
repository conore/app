import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    item: {

    },
    noResult: {
        marginTop: 150,
        alignItems: 'center', 
        justifyContent: 'center',
    },
    noResultTxt: {
        textAlign: "center",
        fontSize: 20,
        marginTop: 15,
    },
    retry:{
        textAlign: "center",
        fontSize: 18,
        marginTop: 5,
        color: "#545454"
    }
})
