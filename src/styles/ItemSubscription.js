import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    abonnement: {
        marginLeft: 25,
        marginRight: 25,
        marginTop: 7,
        marginBottom: 7,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 10,
        paddingBottom: 10,
        elevation: 1,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#e8e8e8",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'center'
    }, 
    abonnementActive: {
        marginLeft: 25,
        marginRight: 25,
        marginTop: 7,
        marginBottom: 7,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 10,
        paddingBottom: 10,
        elevation: 1,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#4a98ff",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'center'
    },
    abonnementEmpty: {
        marginLeft: 25,
        marginRight: 25,
        marginTop: 7,
        marginBottom: 7,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 10,
        paddingBottom: 10,
        elevation: 1,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#e8e8e8",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: 'center'
    }, 
    abonnementDetailsEmpty: {
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 15,
        paddingBottom: 15,
        elevation: 1,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#e8e8e8",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: 'center'
    }, 
    emptySubTxt: {
        fontSize: 18,
        color: "#c7c7c7"
    },
    title: {
        fontSize: 18,
    },
    subIcon: {
        flexDirection: "row",
        alignItems: 'center'
    },
    subTitleBlock: {
        backgroundColor: '#dff8ed',
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: 'center',
        borderRadius: 15
    },
    subTitleBlockActive: {
        backgroundColor: '#4a98ff2e',
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: 'center',
        borderRadius: 15
    },
    subTitle: {
        color: '#51c794',
        fontSize: 15
    },
    subTitleActive: {
        color: '#4a98ff',
        fontSize: 15
    },
    price: {
        color: '#ffb016',
        fontSize: 15
    }, 
    priceActive: {
        color: '#4a98ff',
        fontSize: 15
    },
    abonnementDetails: {
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 15,
        paddingBottom: 15,
        elevation: 1,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#e8e8e8",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'center'
    }, 
    subTitleBlockWarning: {
        backgroundColor: '#ffd04a40',
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: 'center',
        borderRadius: 15
    },
    subTitleBlockAlert: {
        backgroundColor: '#ff4a4a2e',
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 15,
        paddingRight: 15,
        alignItems: 'center',
        borderRadius: 15
    },
    subTitleWarning: {
        color: '#ffb016',
        fontSize: 15
    },
    subTitleAlert: {
        color: '#ff4a4a',
        fontSize: 15
    },
})
