import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    marginLeft: 30,
    marginRight: 30,
  },
  title: {
      fontSize: 27,
      marginTop: 40,
      fontWeight: 'bold'
  },
  subtitle: {
      fontSize: 22,
      marginTop: 10,
      color: '#858585',
  },
  submitBtn: {
      borderRadius: 5,
      backgroundColor: '#4960de',
      marginTop: 10,
      marginBottom: 20,
      paddingRight: 25,
      paddingLeft: 25,
      paddingTop: 10,
      paddingBottom: 10,
  }, 
  submitBtnTxt: {
      textAlign: 'center',
      color: '#fff',
      fontSize: 20,
      fontWeight: 'bold'
  },
  backLoginTxt: {
      textAlign: 'center',
      color: '#4960de',
      fontSize: 20,
      marginBottom: 30,
  }
})
