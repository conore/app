import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    containerReserver:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    containerList: {
        flex: 1,
        flexDirection: 'row',
    },
    reservationBtn:{
        backgroundColor: '#5e72e4',
        paddingTop: 15,
        paddingBottom: 15,
        justifyContent: 'flex-end',
        elevation: 1
    },
    annulationBtn:{
        backgroundColor: '#e04c4c',
        paddingTop: 15,
        paddingBottom: 15,
        justifyContent: 'flex-end',
        elevation: 1
    },
    reservationTxt:{
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    },
})
