import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    title: {
        fontSize: 27,
        marginTop: 35,
        marginLeft: 20,
        marginRight: 20,
        fontWeight: 'bold'
    },
    subtitle: {
        fontSize: 22,
        marginTop: 10,
        marginBottom: 35,
        marginLeft: 20,
        marginRight: 20,
        color: '#858585',
    },
    titleClub: {
        fontSize: 20,
        paddingLeft: 15,
        paddingBottom: 10
    },
    otherClub: {
        paddingTop: 15,
        borderTopWidth: 1,
        borderColor: "#e8e8e8",
    },
    confirmBtnActive: {
        borderRadius: 5,
        backgroundColor: '#4960de',
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        paddingRight: 25,
        paddingLeft: 25,
        paddingTop: 10,
        paddingBottom: 10,
    }, 
    confirmBtnTxtActive: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },
    confirmBtn: {
        borderRadius: 5,
        backgroundColor: '#f0f0f0',
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        paddingRight: 25,
        paddingLeft: 25,
        paddingTop: 10,
        paddingBottom: 10,
    }, 
    confirmBtnTxt: {
        textAlign: 'center',
        color: '#c7c7c7',
        fontSize: 20,
        fontWeight: 'bold'
    },
    backLoginTxt: {
        textAlign: 'center',
        color: '#4960de',
        fontSize: 20,
        marginTop: 10,
        marginBottom: 30,
    }
})
