import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    view: {
        flex: 0.5,
    },
    titleBlock: {
        backgroundColor: "#edf1f7",
        borderRadius: 5,
        paddingTop: 20,
        paddingBottom: 20
    },
    imgProfile:{
        width: 120, 
        height: 120, 
        borderRadius: 100,
        alignSelf: 'center'
    },
    titleProfile:{
        marginTop: 15,
        fontSize: 22,
        alignSelf: 'center'
    },
    nameTxt:{
        fontSize: 17,
        color: "#4f4f4f",
        alignSelf: 'center'
    }
})
