
import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    view: {
        flex: 1
    },
    clubsBlock: {
        marginTop: 40,
        marginLeft: 35,
        marginRight: 35,
        paddingLeft: 25,
        paddingRight: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: "#e8e8e8",
        borderRadius: 7,
    },
    settingsBlock: {
        marginTop: 20,
        marginLeft: 35,
        marginRight: 35,
        paddingLeft: 25,
        paddingRight: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: "#e8e8e8",
        borderRadius: 7,
    },
    clubsTitle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    statsBlock: {
        marginTop: 15,
        paddingLeft: 20,
        paddingRight: 20,
    },
    label:{
        marginLeft: 20,
        fontSize: 18,
    },
    arrowBtn: {
        alignItems: 'center'
    },
    logoutBlock: {
        marginTop: 30,
        marginLeft: 35,
        marginRight: 35,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: "#e04c4c",
        borderRadius: 7,
    },
    logout: {
        fontSize: 17,
        textAlign: 'center',
        color: "#e04c4c"
    },
})