import { StyleSheet, Platform } from 'react-native'

export default styles = StyleSheet.create({
    topBar: {
        height: 60,
        backgroundColor: '#fff',
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#d6d6d6',
        shadowColor: '#d6d6d65e',
        elevation: 2,
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 2,
        shadowOpacity: 1.0
    },
    topBarTxt:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#5e72e4',
        ...Platform.select({
            ios: {
              textAlign: 'center'
            },
            android: {
                marginLeft: 15
            },
          }),
    }
})
