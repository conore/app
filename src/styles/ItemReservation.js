import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    item: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    itemFull:{
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#fff"
    },
    hours:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#33335d'
    },
    duration:{
        fontSize: 19,
        color: '#94a2b3'
    },
    placesBlock:{
       backgroundColor: '#dff8ed',
       width: 170,
       paddingTop: 6,
       paddingBottom: 6,
       paddingLeft: 10,
       paddingRight: 10,
       alignItems: 'center',
       borderRadius: 15
    },
    places:{
        color: '#51c794',
        fontSize: 15
    },
    hoursFull:{
        fontSize: 19,
        fontWeight: 'bold',
        color: '#33335d'
    },
    durationFull:{
        fontSize: 19,
        color: '#9e9e9e'
    },
    nameFull:{
        fontSize: 19,
        color: '#9e9e9e'
    },
    placesBlockFull:{
       width: 170,
       paddingTop: 5,
       paddingBottom: 5,
       alignItems: 'center',
       borderRadius: 15
    },
    placesFull:{
        color: '#9e9e9e',
        fontSize: 16
    },
    placesBlockReserve:{
        backgroundColor: '#D62D2D10',
        width: 170,
        paddingTop: 5,
        paddingBottom: 5,
        alignItems: 'center',
        borderRadius: 15
    },
    placesReserve:{
        color: '#B44B4B',
        fontSize: 16
    },
    itemSelected:{
        backgroundColor: '#5e72e4'
    },
    itemAnnuler:{
        backgroundColor: '#e04c4c'
    },
    hoursSelect:{
        color: '#fff'
    },
    durationSelect:{
        color: '#fff'
    },
    placesBlockSelected:{
        backgroundColor: '#ffffff25'
    },
    placesBlockAnnuler:{
        backgroundColor: '#ffffff25'
    },
    placesSelected:{
        color: '#fff'
    }
})
