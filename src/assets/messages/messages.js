export default {
    // Auth
    "auth/invalid-email": "Le format de votre email est invalide",
    "auth/invalid-form": "Votre email ou votre mot de passe est invalide",
    "auth/invalid-new-password": "Le mot de passe doit contenir 6 caractères minimum",
    "auth/invalid-form-empty": "Veuillez saisir tous les champs",
    "auth/invalid-confirm-password": "Le mot de passe et la confirmation doivent être identique",
    "auth/wrong-email": "L'adresse email est déjà utilisé",
    "auth/error-logout": "Une erreur est survenu lors de la deconnexion",

    // General
    "general/error": "Une erreur inconnue s'est produite...",
    //"general/error-network": "Vérifiez votre connexion Internet",
    "general/error-network": "Le service est momentanément indisponible"
}