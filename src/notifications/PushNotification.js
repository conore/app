import React, { Component } from 'react'
import { AsyncStorage } from 'react-native'
import firebase from 'react-native-firebase'

import ApiRequest from '../api/ApiRequest'

export default class PushNotification extends Component {

    componentDidMount() {
        this.checkPermission()
    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission()
        if (enabled) {
            this.getToken()
        } else {
            this.requestPermission()
        }
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission()
            // User has authorised
            this.getToken()
        } catch (error) {
            console.log('permission rejected')
        }
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken')
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken()
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken)
            }
        }
        console.log("FCMToken:", fcmToken)
        ApiRequest.pushNotifToken(fcmToken, "").then(res => {
            if(res.data.status == "success") {
                console.log("token pushed !")
            }
        })
    }

    render() {
        return null
    }
}