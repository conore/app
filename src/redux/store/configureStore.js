import { createStore } from 'redux'
import toggleSwitchClubs from './reducers/switchClubs'

export default createStore(toggleSwitchClubs)