const initialState = { clubId: null}

function toggleSwitchClubs(state = initialState, action) {
    let nextState

    switch (action.type) {
        case 'TOOGLE_SWITCH_CLUB':
            nextState = {
                ...state,
                clubId: action.value
            }
            return nextState
        default:
            return state
    }
}

export default toggleSwitchClubs