import React, { Component } from 'react'
import Navigation from './src/navigation/index.js'
import { Provider } from 'react-redux'
import Store from './src/redux/store/configureStore'
import { View, StyleSheet, Text } from 'react-native'
import moment from 'moment'
import ItemReservation from './src/components/utils/ItemReservation.js'
import ApiRequest from './src/api/ApiRequest.js'

import CheckAlert from "react-native-awesome-alert"
import firebase from 'react-native-firebase'
import 'react-native-gesture-handler'

const AlertRender = (event_at, places, session_id, duration, day) => (
  <View>
     <View style={styles.dateBarContainer}>
      <Text style={styles.titleDate}>{day}</Text>
    </View>

    <View style={{ marginTop: 10, marginBottom: 10}}>
      <ItemReservation
        id={session_id} 
        hours={moment.unix(event_at).format('HH[h]mm')} 
        duration={duration} 
        places={places} 
        hasReservated={false} 
        selected={false}
      />
    </View>
  </View>
)

export default class App extends Component {

  componentDidMount() {
    this.createNotificationListeners()
  }

  componentWillUnmount() {
    this.notificationListener()
    this.notificationOpenedListener()
  }

  async createNotificationListeners() {

    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { club_name, club_id, event_at, places, session_id , duration } = notification._data
      this.showAlert(`${club_name} - Nouvelle séance disponible`, club_id, event_at, places, session_id, duration)
    });
     
      this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        const { club_name, club_id, event_at, places, session_id , duration } = notificationOpen.notification._data
        this.showAlert(`${club_name} - Nouvelle séance disponible`, club_id, event_at, places, session_id, duration)
      })
     
      const notificationOpen = await firebase.notifications().getInitialNotification()
      if (notificationOpen) {
        const { club_name, club_id, event_at, places, session_id , duration } = notificationOpen.notification._data
        this.showAlert(`${club_name} - Nouvelle séance disponible`, club_id, event_at, places, session_id, duration)
      }
     
      this.messageListener = firebase.messaging().onMessage((message) => {
        console.log(JSON.stringify(message))
      })
  }

  showAlert(title, club_id, event_at, places, session_id, duration) {
    let titleDays = moment.unix(event_at).format("dddd D MMMM")
    let daySplit = titleDays.split(" ")
    let dayName = daySplit[0].charAt(0).toUpperCase() + daySplit[0].slice(1)
    let day =  `${dayName} ${daySplit[1]} ${daySplit[2]}`

    this.checkAlert.alert(title, AlertRender(event_at, places, session_id, duration, day), [
      { text: "Pas maintenant", onPress: () => console.log("Do nothing"), style: { color: 'gray' } },
      { text: "Reserver", onPress: () => ApiRequest.setReservation(club_id, session_id), style: { color: '#5e72e4' } },
    ])
  }

  render() {
    return (
      <Provider store={Store}>
        <CheckAlert
          styles={{
            modalView: { backgroundColor: "#fff", borderRadius: 5, width: 350 },
            titleText: { fontSize: 20, fontWeight: 'bold', padding: 15, width: 350, alignSelf: "flex-start", borderBottomWidth: StyleSheet.hairlineWidth, borderColor: '#d6d6d6' },
            buttonContainer: { justifyContent: 'space-between', paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5, borderTopWidth: StyleSheet.hairlineWidth, borderColor: '#d6d6d6' }
          }}
          ref={ref => (this.checkAlert = ref)}
          modalProps={{
            transparent: true,
            animationType: 'fade'
          }}
        />
        <Navigation />
      </Provider>
    )
  }
}

const styles = StyleSheet.create({
  dateBarContainer: {
      flexDirection: "row",
      backgroundColor: '#5e72e412',
      justifyContent: "center",
      alignItems: "center",
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      height: 60, 
  },
  titleDate: {
      fontSize: 19,
      color: "#5e72e4",
      fontWeight: "bold"
  },
})
