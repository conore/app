var merge = require('merge-package-json');
var fs = require('fs');

var dst = fs.readFileSync('./package.json')
var src = fs.readFileSync('./conoreDev/package.json')
var json = merge(dst, src)

fs.writeFile("./conoreDev/package.json", json, function(err) {
  if(err) {
    return console.log(err);
  }
  console.log("The file was saved!");
})